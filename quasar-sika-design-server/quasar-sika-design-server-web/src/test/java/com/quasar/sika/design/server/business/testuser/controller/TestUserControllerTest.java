package com.quasar.sika.design.server.business.testuser.controller;

import com.quasar.sika.design.server.business.testuser.pojo.query.TestUserQuery;
import com.sika.code.common.json.util.JSONUtil;
import com.sika.code.standard.base.test.BaseControllerTest;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@Slf4j
public class TestUserControllerTest extends BaseControllerTest {

    @Test
    public void find() throws Exception {
        //创建新用户
        TestUserQuery userParam = new TestUserQuery();
        userParam.setUserId(42012L);
        //将参数转换成JSON对象
        String json = JSONUtil.toJSONString(userParam);
        //执行请求（使用POST请求，传递对象参数）
        MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.post("/test_user/find/anon").content(json).contentType(MediaType.APPLICATION_JSON)).andReturn();
        //获取返回编码
        int status = mvcResult.getResponse().getStatus();
        //获取返回结果
        String content = mvcResult.getResponse().getContentAsString();
        //断言，判断返回编码是否正确
        Assert.assertEquals(200, status);
        log.info("content:{}", content);
        //断言，判断返回结果是否正确
//        Assert.assertEquals("true", content);
    }

}
