package com.quasar.sika.design.server.business.testuser.mapper;

import com.quasar.sika.design.server.business.testuser.entity.TestUserEntity;
import com.quasar.sika.design.server.business.testuser.pojo.query.TestUserQuery;
import com.sika.code.standard.base.test.BaseMapperTest;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class TestUserMapperTest extends BaseMapperTest {
    @Autowired
    private TestUserMapper testUserMapper;

    @Test
    public void findByQuery() {
        TestUserQuery testUserQuery = new TestUserQuery();
        testUserQuery.setUserId(42012L);
        TestUserEntity testUserEntity = testUserMapper.findByQuery(testUserQuery);
        Assert.assertEquals(testUserQuery.getUserId(), testUserEntity.getUserId());
    }
}