package com.quasar.sika.design.server.business.testuser.service;

import com.quasar.sika.design.server.business.testuser.pojo.dto.TestUserDTO;
import com.quasar.sika.design.server.business.testuser.pojo.query.TestUserQuery;
import com.sika.code.standard.base.test.BaseServiceTest;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class TestUserServiceTest extends BaseServiceTest {
    @Autowired
    private TestUserService testUserService;
    @Test
    public void testFind() {
        TestUserQuery query = new TestUserQuery();
        query.setUserId(42012L);
        TestUserDTO testUserDTO = testUserService.find(query);
        Assert.assertEquals(query.getUserId(), testUserDTO.getUserId());
    }
}
