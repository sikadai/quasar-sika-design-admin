package com.quasar.sika.design.server.business.testuser.controller;


import cn.hutool.core.util.StrUtil;
import com.google.common.collect.Lists;
import com.quasar.sika.design.server.business.testuser.pojo.dto.TestUserDTO;
import com.quasar.sika.design.server.business.testuser.pojo.query.TestUserQuery;
import com.quasar.sika.design.server.business.testuser.service.TestUserService;
import com.quasar.sika.design.server.business.user.pojo.query.UserQuery;
import com.quasar.sika.design.server.business.user.service.UserService;
import com.sika.code.basic.constant.BaseConstant;
import com.sika.code.result.Result;
import com.sika.code.standard.base.controller.BaseStandardController;
import com.sika.code.standard.db.util.HintManagerHandler;
import org.apache.shardingsphere.api.hint.HintManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * <p>
 * 测试用户表 前端控制器
 * </p>
 *
 * @author daiqi
 * @since 2021-07-03 15:44:06
 */
@RestController(value = "testUserController")
@RequestMapping("test_user")
public class TestUserController extends BaseStandardController {
    @Autowired
    private TestUserService testUserService;
    @Autowired
    private UserService userService;

    @RequestMapping(value = "save/anon")
    public Result save(@RequestBody TestUserDTO testUserDto) {
        return super.success(testUserService.save(testUserDto));
    }

    @RequestMapping(value = "save_batch/anon")
    public Result saveBatch(@RequestBody List<TestUserDTO> testUserDtos) {
        testUserDtos = Lists.newArrayList();
        for (int i = 0 ; i < 1000; ++i) {
            TestUserDTO testUserDTO = new TestUserDTO();
            String value = (10000+i) + "012";
            testUserDTO.setUserId(Long.valueOf(value));
            testUserDTO.setUserName(UUID.randomUUID().toString());
            testUserDTO.setIsDeleted(BaseConstant.IsDeletedEnum.NO.getType());
            testUserDTO.setCreateBy(StrUtil.EMPTY);
            testUserDTO.setUpdateBy(StrUtil.EMPTY);
            testUserDTO.setVersion(0);
            testUserDTO.setUpdateDate(new Date());
            testUserDTO.setCreateDate(new Date());
            testUserDTO.setAvailable(0);
            testUserDTO.setRemark("");
            testUserDtos.add(testUserDTO);
        }
        Result result;
        try (HintManager hintManager = HintManagerHandler.getInstanceBeforeClear()) {
            HintManagerHandler.addDatabaseShardingValue(hintManager,"01");
            HintManagerHandler.addTableShardingValue(hintManager, "2");
            result = super.success(testUserService.saveForBatch(testUserDtos));
        }
        return result;
    }

    @RequestMapping(value = "update_by_id/anon")
    public Result updateById(@RequestBody TestUserDTO testUserDto) {
        return super.success(testUserService.updateById(testUserDto));
    }

    @RequestMapping(value = "page/anon")
    public Result page(@RequestBody TestUserQuery testUserQuery) {
        Result result;
        try (HintManager hintManager = HintManagerHandler.getInstanceBeforeClear()) {
            HintManagerHandler.addDatabaseShardingValue(hintManager,"01");
            HintManagerHandler.addTableShardingValue(hintManager, "2");
            result = super.success(testUserService.page(testUserQuery));
        }
        return result;
    }

    @RequestMapping(value = "find/anon")
    public Result find(@RequestBody TestUserQuery testUserQuery) {
        return super.success(testUserService.find(testUserQuery));
    }

    @RequestMapping(value = "find/user/anon")
    public Result findUser(@RequestBody UserQuery userQuery) {
        return super.success(userService.findByPrimaryKey(userQuery.getId()));
    }

    @RequestMapping(value = "list/anon")
    public Result list(@RequestBody TestUserQuery testUserQuery) {
        Result result;
        try (HintManager hintManager = HintManagerHandler.getInstanceBeforeClear()) {
            HintManagerHandler.addDatabaseShardingValue(hintManager,"01");
            HintManagerHandler.addTableShardingValue(hintManager, "2");
            result = super.success(testUserService.list(testUserQuery));
        }
        return result;
    }
}
