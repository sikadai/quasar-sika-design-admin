package com.sika.code.standard.db.util;

import cn.hutool.core.util.StrUtil;
import org.apache.shardingsphere.api.hint.HintManager;

import java.util.Collection;

/**
 * @author daiqi
 * @create 2021-10-11 14:04
 */
public class HintManagerHandler {
    private static final String LOGIC_NAME_DEFAULT = "SIKA:LOGIC:NAME:DEFAULT";

    public static HintManager getInstance() {
        return HintManager.getInstance();
    }

    public static HintManager getInstanceBeforeClear() {
        clear();
        return HintManager.getInstance();
    }

    public static void setDatabaseShardingValueEmpty(final HintManager hintManager, final Comparable<?> value) {
        hintManager.setDatabaseShardingValue(value);
    }

    public static void addDatabaseShardingValue(final HintManager hintManager, final Comparable<?> value) {
        hintManager.addDatabaseShardingValue(LOGIC_NAME_DEFAULT, value);
    }

    public static void addTableShardingValue(final HintManager hintManager, final Comparable<?> value) {
        hintManager.addTableShardingValue(LOGIC_NAME_DEFAULT, value);
    }

    public static Collection<Comparable<?>> getDatabaseShardingValuesEmpty() {
        return HintManager.getDatabaseShardingValues(StrUtil.EMPTY);
    }

    public static Collection<Comparable<?>> getDatabaseShardingValues() {
        return HintManager.getDatabaseShardingValues(LOGIC_NAME_DEFAULT);
    }

    public static Collection<Comparable<?>> getTableShardingValues() {
        return HintManager.getTableShardingValues(LOGIC_NAME_DEFAULT);
    }

    public static boolean isDatabaseShardingOnly() {
        return HintManager.isDatabaseShardingOnly();
    }

    public static void setMasterRouteOnly(HintManager hintManager) {
        hintManager.setMasterRouteOnly();
    }

    public static boolean isMasterRouteOnly() {
        return HintManager.isMasterRouteOnly();
    }

    public static void clear() {
        HintManager.clear();
    }

    public static void close(HintManager hintManager) {
        hintManager.close();
    }

}
