package com.sika.code.standard.db.util;

import com.sika.code.common.util.ReflectionUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.shardingsphere.api.config.sharding.strategy.ComplexShardingStrategyConfiguration;
import org.apache.shardingsphere.api.config.sharding.strategy.ShardingStrategyConfiguration;

/**
 * @author sikadai
 * @Description:
 * @date 2021/7/412:42
 */
@Getter
@AllArgsConstructor
public class CustomerStandardShardingStrategyConfiguration  implements ShardingStrategyConfiguration {
    private final String shardingColumn;
    private final String algorithmClassName;

    public ShardingStrategyConfiguration build() {
//        return this;
//        return new StandardShardingStrategyConfiguration(shardingColumn, ReflectionUtil.newInstance(algorithmClassName));
        return new ComplexShardingStrategyConfiguration(shardingColumn, ReflectionUtil.newInstance(algorithmClassName));
//        return new HintShardingStrategyConfiguration(ReflectionUtil.newInstance(algorithmClassName));
    }
}
