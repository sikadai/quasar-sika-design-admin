package com.sika.code.standard.db.algorithm.table;

import com.sika.code.common.date.constant.DateConstant;
import com.sika.code.common.date.util.DateUtil;
import com.sika.code.standard.db.algorithm.ShardingValueContext;

import java.util.Date;

/**
 * @author sikadai
 * @Description:年库天表分片算法
 * @date 2021/7/415:16
 */
public class YearDayShardingAlgorithm extends BaseTableShardingAlgorithm<Date> {

    @Override
    public String getGetDataSourceNameFromShardKey(ShardingValueContext<Date> shardingValue) {
        String year = DateUtil.format(shardingValue.getValues().iterator().next(), DateConstant.YEAR);
        return formatExpression(getDataBaseNameExpression(shardingValue.getLogicTableName()), year);
    }

    @Override
    public String getGetTableNameFromShardKey(ShardingValueContext<Date> shardingValue) {
        String monthAndDay = DateUtil.format(shardingValue.getValues().iterator().next(), DateConstant.MONTH_DAY);
        return formatExpression(getTableNameExpression(shardingValue.getLogicTableName()), monthAndDay);
    }
}
