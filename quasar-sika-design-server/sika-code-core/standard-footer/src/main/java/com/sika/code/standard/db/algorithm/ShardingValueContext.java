package com.sika.code.standard.db.algorithm;

import lombok.Data;
import lombok.experimental.Accessors;
import org.apache.shardingsphere.api.sharding.ShardingValue;

import java.util.Collection;

@Data
@Accessors(chain = true)
public class ShardingValueContext<T extends Comparable<?>> implements ShardingValue {
    private Collection<String> availableTargetNames;
    private String logicTableName;

    private String columnName;

    private Collection<T> values;
}