package com.sika.code.standard.db.algorithm.datasource;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.sika.code.standard.db.algorithm.BaseShardingAlgorithm;
import com.sika.code.standard.db.algorithm.ShardingValueContext;
import lombok.extern.slf4j.Slf4j;
import org.apache.shardingsphere.api.sharding.complex.ComplexKeysShardingValue;
import org.apache.shardingsphere.api.sharding.hint.HintShardingValue;
import org.apache.shardingsphere.api.sharding.standard.PreciseShardingValue;

import java.util.Collection;
import java.util.Map;

/**
 * <p>
 * 基础数据源分片算法
 * </p>
 * <pre>
 *     这个不是查询分片算法-是匹配数据源的算法
 * </pre>
 *
 * @author daiqi
 * @date 2018/12/4 9:40
 */
@Slf4j
public class DefaultDataSourceShardingAlgorithm<T extends Comparable<?>> implements BaseShardingAlgorithm<T> {
    private static final Map<String, String> TABLE_TO_DB_SOURCE = Maps.newConcurrentMap();

    @Override
    public Collection<String> doSharding(Collection<String> availableTargetNames, ComplexKeysShardingValue<T> shardingValue) {
        ShardingValueContext<T> shardingValueContext = buildShardingValueContext(availableTargetNames, shardingValue);
        return getDbSources(shardingValueContext);
    }

    @Override
    public String doSharding(Collection<String> availableTargetNames, PreciseShardingValue<T> shardingValue) {
        ShardingValueContext<T> shardingValueContext = buildShardingValueContext(availableTargetNames, shardingValue);
        return getDbSources(shardingValueContext).iterator().next();
    }

    public static void addTableToDbSource(String logicTableName, String dbSource) {
        TABLE_TO_DB_SOURCE.put(logicTableName, dbSource);
    }

    @Override
    public Collection<String> doSharding(Collection<String> availableTargetNames, HintShardingValue<T> shardingValue) {
        ShardingValueContext<T> shardingValueContext = buildShardingValueContext(availableTargetNames, shardingValue);
       return getDbSources(shardingValueContext);
    }

    private Collection<String> getDbSources(ShardingValueContext<T> shardingValue) {
        String logicTableName = shardingValue.getLogicTableName();
        String dbSource = TABLE_TO_DB_SOURCE.get(logicTableName);
        if (!shardingValue.getAvailableTargetNames().contains(dbSource)) {
            throw new IllegalArgumentException();
        }
        return Lists.newArrayList(dbSource);
    }
}