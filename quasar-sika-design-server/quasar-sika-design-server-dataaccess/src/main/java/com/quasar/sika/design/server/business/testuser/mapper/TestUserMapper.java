package com.quasar.sika.design.server.business.testuser.mapper;

import com.quasar.sika.design.server.business.testuser.entity.TestUserEntity;
import com.sika.code.standard.base.basemapper.BaseStandardMapper;
import org.springframework.stereotype.Repository;

import java.util.Collection;

/**
 * <p>
 * 测试用户表 Mapper 接口
 * </p>
 *
 * @author daiqi
 * @since 2021-07-03 15:44:04
 */
@Repository
public interface TestUserMapper extends BaseStandardMapper<TestUserEntity> {
    boolean saveBatch(Collection<TestUserEntity> entities);
}
