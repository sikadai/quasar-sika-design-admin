package com.sika.code.infractructure.db.properties;

import cn.hutool.core.util.StrUtil;
import lombok.Data;

/**
 * @author sikadai
 * @Description: 分片Item
 * @date 2021/7/417:59
 */
@Data
public class ShardingItem {
    private static final String SHARDING_PLACEHOLDER_COLUMN_DEFAULT = "is_deleted,id";
    private String shardingPlaceholderColumn;
    private String shardingColumn;
    private String dataSourceName;
    private String dataBaseNameExpression;
    private String tableNameExpression;
    private String dataSourceAlgorithmClassName;
    private String tableAlgorithmClassName;

    public ShardingItem build() {
        if (StrUtil.isBlank(shardingPlaceholderColumn)) {
            this.shardingPlaceholderColumn = SHARDING_PLACEHOLDER_COLUMN_DEFAULT;
        }
        return this;
    }
}
