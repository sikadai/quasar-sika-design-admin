package com.sika.code.infractructure.db.algorithm.table;

import cn.hutool.core.util.StrUtil;
import com.sika.code.infractructure.db.algorithm.ShardingValueContext;

/**
 * @author sikadai
 * @Description:百库十表分片算法
 * @date 2021/7/415:16
 */
public class HundredTenShardingAlgorithm extends BaseTableShardingAlgorithm<Long> {

    @Override
    public String getGetDataSourceNameFromShardKey(ShardingValueContext<Long> shardingValue) {
        String hundred = StrUtil.fillBefore(String.valueOf(shardingValue.getValues().iterator().next() % 1000 / 10), FILLED_CHAR_ZERO, LENGTH_TWO);
        return formatExpression(getDataBaseNameExpression(shardingValue.getLogicTableName()), hundred);
    }

    @Override
    public String getGetTableNameFromShardKey(ShardingValueContext<Long> shardingValue) {
        String ten = String.valueOf(shardingValue.getValues().iterator().next() % 10);
        return formatExpression(getTableNameExpression(shardingValue.getLogicTableName()), ten);
    }
}

