package com.sika.code.infractructure.db.algorithm.table;


import cn.hutool.core.date.DateUtil;
import com.sika.code.infractructure.db.algorithm.ShardingValueContext;

import java.util.Date;

/**
 * @author sikadai
 * @Description:年库天表分片算法
 * @date 2021/7/415:16
 */
public class YearDayShardingAlgorithm extends BaseTableShardingAlgorithm<Date> {

    @Override
    public String getGetDataSourceNameFromShardKey(ShardingValueContext<Date> shardingValue) {
        String year = DateUtil.format(shardingValue.getValues().iterator().next(), YEAR);
        return formatExpression(getDataBaseNameExpression(shardingValue.getLogicTableName()), year);
    }

    @Override
    public String getGetTableNameFromShardKey(ShardingValueContext<Date> shardingValue) {
        String monthAndDay = DateUtil.format(shardingValue.getValues().iterator().next(), MONTH_DAY);
        return formatExpression(getTableNameExpression(shardingValue.getLogicTableName()), monthAndDay);
    }
}
