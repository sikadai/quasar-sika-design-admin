package com.sika.code.infractructure.db.repository.impl;

import cn.hutool.extra.spring.SpringUtil;
import com.baomidou.mybatisplus.core.toolkit.ReflectionKit;
import com.sika.code.infractructure.db.mapper.BaseMapper;
import com.sika.code.infractructure.db.repository.BaseRepositoryMybatisPlus;
import com.sika.code.infrastructure.common.base.pojo.po.BasePO;

import java.io.Serializable;

/**
 * @author daiqi
 * @create 2021-10-19 15:21
 */
public abstract class BaseRepositoryMyBatisPlus<PRIMARY extends Serializable, PO extends BasePO<PRIMARY>, Mapper extends BaseMapper<PRIMARY, PO>> implements BaseRepositoryMybatisPlus<PRIMARY, PO, Mapper> {

    /**
     * 只需要动态获取一次即可
     */
    protected Mapper mapper = currentMapper();

    protected Mapper currentMapper() {
        return SpringUtil.getBean(currentMapperClass());
    }

    protected Class<Mapper> currentMapperClass() {
        return (Class<Mapper>) getSuperClassGenericType(2);
    }

    private Class<?> getSuperClassGenericType(int index) {
        return ReflectionKit.getSuperClassGenericType(this.getClass(), BaseRepositoryMybatisPlus.class, index);
    }

    @Override
    public Mapper getMapper() {
        return this.mapper;
    }
}
