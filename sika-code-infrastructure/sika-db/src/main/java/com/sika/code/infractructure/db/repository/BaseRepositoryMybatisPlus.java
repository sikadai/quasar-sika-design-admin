package com.sika.code.infractructure.db.repository;

import cn.hutool.core.collection.CollUtil;
import com.sika.code.infractructure.db.mapper.BaseMapper;
import com.sika.code.infrastructure.common.base.pojo.po.BasePO;
import com.sika.code.infrastructure.common.base.pojo.query.BaseQuery;
import com.sika.code.infrastructure.common.base.pojo.query.PageQuery;
import com.sika.code.infrastructure.common.base.repository.BaseRepository;
import org.assertj.core.util.Lists;

import java.io.Serializable;
import java.security.InvalidParameterException;
import java.util.List;

/**
 * mybatis基础持久化接口
 *
 * @author daiqi
 * @create 2021-10-15 22:39
 */
public interface BaseRepositoryMybatisPlus<PRIMARY extends Serializable, PO extends BasePO<PRIMARY>, Mapper extends BaseMapper<PRIMARY, PO>> extends BaseRepository<PRIMARY, PO> {

    Mapper getMapper();

    @Override
    default PO findByPrimaryKey(PRIMARY primaryKey) {
        return getMapper().selectById(primaryKey);
    }

    @Override
    default PRIMARY saveRetId(PO po) {
        if (po == null) {
            throw new InvalidParameterException("持久化对象PO不能为空");
        }
        if (po.getId() == null) {
            getMapper().insert(po);
            return po.getId();
        } else {
            int count = getMapper().updateById(po);
            if (count > 0) {
                return po.getId();
            }
            throw new RuntimeException("数据更新失败");
        }
    }

    @Override
    default int save(PO po) {
        if (po == null) {
            throw new InvalidParameterException("持久化对象PO不能为空");
        }
        if (po.getId() == null) {
            return getMapper().insert(po);
        } else {
            return getMapper().updateById(po);
        }
    }

    @Override
    default int saveBatch(List<PO> pos) {
        int count = 0;
        if (CollUtil.isEmpty(pos)) {
            return count;
        }
        List<PO> waitForInsert = Lists.newArrayList();
        List<PO> waitForUpdate = Lists.newArrayList();
        for (PO po : pos) {
            if (po.getId() == null) {
                waitForInsert.add(po);
            } else {
                waitForUpdate.add(po);
            }
        }
        if (CollUtil.isNotEmpty(waitForInsert)) {
            count += insertBatch(waitForInsert);
        }
        if (CollUtil.isNotEmpty(waitForUpdate)) {
            count += updateBatch(waitForUpdate);
        }
        return count;
    }

    @Override
    default <QUERY extends BaseQuery<PRIMARY>> PO find(QUERY query) {
        return getMapper().find(query);
    }

    @Override
    default <QUERY extends BaseQuery<PRIMARY>> PRIMARY findId(QUERY query) {
        return getMapper().findId(query);
    }

    @Override
    default <QUERY extends BaseQuery<PRIMARY>> List<PO> list(QUERY query) {
        return getMapper().list(query);
    }

    @Override
    default <QUERY extends BaseQuery<PRIMARY>> List<PRIMARY> listId(QUERY query) {
        return getMapper().listId(query);
    }

    @Override
    default <QUERY extends PageQuery<PRIMARY>> List<PO> page(QUERY query) {
        return getMapper().page(query);
    }

    @Override
    default <Query extends BaseQuery<PRIMARY>> int count(Query query) {
        return getMapper().count(query);
    }
}
