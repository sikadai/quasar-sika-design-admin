package com.sika.code.infractructure.db.util;

import com.sika.code.infrastructure.common.util.BeanUtil;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.shardingsphere.api.config.sharding.strategy.ComplexShardingStrategyConfiguration;
import org.apache.shardingsphere.api.config.sharding.strategy.ShardingStrategyConfiguration;

/**
 * @author sikadai
 * @Description:
 * @date 2021/7/412:42
 */
@Getter
@AllArgsConstructor
public class CustomerStandardShardingStrategyConfiguration  implements ShardingStrategyConfiguration {
    private final String shardingColumn;
    private final String algorithmClassName;

    public ShardingStrategyConfiguration build() {
//        return this;
//        return new StandardShardingStrategyConfiguration(shardingColumn, ReflectionUtil.newInstance(algorithmClassName));
        return new ComplexShardingStrategyConfiguration(shardingColumn, BeanUtil.newInstance(algorithmClassName));
//        return new HintShardingStrategyConfiguration(ReflectionUtil.newInstance(algorithmClassName));
    }
}
