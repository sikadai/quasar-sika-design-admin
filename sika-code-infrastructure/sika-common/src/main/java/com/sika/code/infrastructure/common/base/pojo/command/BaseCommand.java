package com.sika.code.infrastructure.common.base.pojo.command;

import com.sika.code.infrastructure.common.base.pojo.BasePoJo;

import java.io.Serializable;

/**
 * 基础command
 *
 * @author daiqi
 * @create 2021-10-13 23:27
 */
public class BaseCommand<PRIMARY extends Serializable> extends BasePoJo<PRIMARY> {
}
