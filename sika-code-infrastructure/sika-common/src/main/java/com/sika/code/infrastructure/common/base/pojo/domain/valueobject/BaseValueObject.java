package com.sika.code.infrastructure.common.base.pojo.domain.valueobject;

import lombok.Data;

/**
 * @author daiqi
 * @create 2021-10-13 0:44
 */
@Data
public class BaseValueObject {
}
