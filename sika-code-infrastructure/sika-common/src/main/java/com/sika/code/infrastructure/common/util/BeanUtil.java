package com.sika.code.infrastructure.common.util;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.ReflectUtil;
import com.google.common.collect.Lists;

import java.util.List;

/**
 * 对象转化者工具类
 */
public class BeanUtil extends cn.hutool.core.bean.BeanUtil {

    public static <INSTANCE> INSTANCE newInstance(String className) {
        Assert.notNull(className, "className不能为空");
        return ReflectUtil.newInstance(className);
    }

    public static <INSTANCE> INSTANCE newInstance(Class<INSTANCE> instanceClass, Object... params) {
        Assert.notNull(instanceClass, "instanceClass不能为空");
        return ReflectUtil.newInstance(instanceClass, params);
    }

    public static <INSTANCE> INSTANCE newInstance(Class<INSTANCE> instanceClass) {
        Assert.notNull(instanceClass, "instanceClass不能为空");
        return ReflectUtil.newInstance(instanceClass);
    }

    public static <TARGET> List<TARGET> toBeans(List<?> sources, Class<TARGET> targetClass) {
        Assert.notNull(targetClass, "targetClass不能为空");
        if (CollUtil.isEmpty(sources)) {
            return Lists.newArrayList();
        }
        List<TARGET> targets = Lists.newArrayList();
        for (Object source : sources) {
            targets.add(toBean(source, targetClass));
        }
        return targets;
    }
}
