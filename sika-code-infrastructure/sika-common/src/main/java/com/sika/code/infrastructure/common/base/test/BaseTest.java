package com.sika.code.infrastructure.common.base.test;

import lombok.Data;
import lombok.Getter;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@SpringBootTest
@RunWith(SpringRunner.class)
@Data
public class BaseTest {
    protected Logger log = LoggerFactory.getLogger(this.getClass());
    protected long testBeginTimeMillis;
    protected long testEndTimeMillis;
    @Before
    public void testBegin() {
        setTestBeginTimeMillis(System.currentTimeMillis());
        log.info("开始执行单元测试");
    }

    @After
    public void testEnd() {
        setTestEndTimeMillis(System.currentTimeMillis());
        log.info("结束执行单元测试，执行时长为{}ms", getTestEndTimeMillis() - getTestBeginTimeMillis());
    }
}
