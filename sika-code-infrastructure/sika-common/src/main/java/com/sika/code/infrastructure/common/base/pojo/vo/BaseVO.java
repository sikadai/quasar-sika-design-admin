package com.sika.code.infrastructure.common.base.pojo.vo;

import com.sika.code.infrastructure.common.base.pojo.BasePoJo;

import java.io.Serializable;

/**
 * 基础视图对象Vo
 *
 * @author daiqi
 */
public class BaseVO<PRIMARY extends Serializable> extends BasePoJo<PRIMARY> {

}
