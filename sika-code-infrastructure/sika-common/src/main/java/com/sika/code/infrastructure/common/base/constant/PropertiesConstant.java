package com.sika.code.infrastructure.common.base.constant;

/**
 * ConfigurationProperties注解的prefix值
 *
 * @author daiqi
 * @create 2019-07-30 9:35
 */
public class PropertiesConstant {
    /**
     * 工作空间 --- sika.code.workspace
     */
    public static final String WORKSPACE = "sika.code.workspace";
    /**
     * 分布式锁属性前缀 --- sika.code.lock
     */
    public static final String LOCK = "sika.code.lock";
    /**
     * 异常前缀 --- sika.code.exception
     */
    public static final String EXCEPTION = "sika.code.exception";
    /**
     * sql前缀 --- sika.code.log.sql
     */
    public static final String LOG_SQL = "sika.code.log.sql";
    /**
     * rabbitMq发送者前缀 --- sika.code.rabbit.sender
     */
    public static final String RABBIT_SENDER = "sika.code.rabbit.sender";
    /**
     * restTemplate信息 --- sika.code.rest.template
     */
    public static final String REST_TEMPLATE = "sika.code.rest.template";
    /**
     * 消息队列发送者确认 --- sika.code.rabbit.sender.ack
     */
    public static final String RABBIT_SENDER_ACK = "sika.code.rabbit.sender.ack";
    /**
     * 日志controller开关 --- sika.code.log.controller.fire
     */
    public static final String LOG_CONTROLLER_FIRE = "sika.code.log.controller.fire";
    /**
     * jdbc开关 --- sika.code.jdbc.fire
     */
    public static final String JDBC_FIRE = "sika.code.jdbc.fire";
    /**
     * redis开关 --- sika.code.redis.fire
     */
    public static final String REDIS_FIRE = "sika.code.redis.fire";
}
