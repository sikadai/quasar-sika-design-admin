package com.sika.code.infrastructure.common.base.pojo.dto;

import com.sika.code.infrastructure.common.base.pojo.BasePoJo;

import java.io.Serializable;

/**
 * 基础数据传输对象
 *
 * @author daiqi
 * @create 2021-10-13 23:27
 */
public class BaseDTO<PRIMARY extends Serializable> extends BasePoJo<PRIMARY> {
}
