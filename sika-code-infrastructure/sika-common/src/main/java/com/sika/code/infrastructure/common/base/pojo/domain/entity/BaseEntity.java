package com.sika.code.infrastructure.common.base.pojo.domain.entity;

/**
 * @author sikadai
 * @Description:基础领域接口
 * @date 2021/4/421:26
 */
public interface BaseEntity<Rsp> {

    /**
     * 执行入口 -- 定义模板执行方式
     */
    Rsp execute();

}
