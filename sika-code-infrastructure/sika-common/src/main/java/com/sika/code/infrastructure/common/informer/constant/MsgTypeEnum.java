package com.sika.code.infrastructure.common.informer.constant;

import com.sika.code.infrastructure.common.base.constant.BaseCodeEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author daiqi
 * @create 2020-05-15 17:13
 */
@AllArgsConstructor
@Getter
public enum MsgTypeEnum implements BaseCodeEnum {
    TEXT("text", "文本消息"),
    MARKDOWN("markdown", "Markdown消息"),
    ;
    private String code;
    private String desc;
}
