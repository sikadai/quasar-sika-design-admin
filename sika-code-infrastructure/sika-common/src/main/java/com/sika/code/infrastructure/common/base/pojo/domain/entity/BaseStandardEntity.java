package com.sika.code.infrastructure.common.base.pojo.domain.entity;

import cn.hutool.core.util.StrUtil;
import com.sika.code.infrastructure.common.log.util.LogUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author sikadai
 * @Description:基础领域接口
 * @date 2021/4/421:26
 */
public abstract class BaseStandardEntity<Rsp> implements BaseEntity<Rsp> {
    /**
     * 获取当前领域对象的日志
     */
    protected Logger logger() {
        return LoggerFactory.getLogger(this.getClass());
    }

    /**
     * 执行入口 -- 定义模板执行方式
     */
    @Override
    public Rsp execute() {
        try {
            executeBefore();
            Rsp rsp = doExecute();
            executeAfter();
            return rsp;
        } catch (Exception e) {
            loggerException(e);
            executeException(e);
        } finally {
            executeFinally();
        }
        return null;
    }


    /**
     * 执行前
     */
    protected void executeBefore() {

    }

    /**
     * 执行者
     */
    protected abstract Rsp doExecute();

    /**
     * 执行后
     */
    protected void executeAfter() {

    }

    protected void loggerException(Exception e) {
        LogUtil.error(StrUtil.format("领域对象【{}】执行异常", this.getClass().getName()), e, logger());
    }

    /**
     * 执行异常
     */
    protected void executeException(Exception e) {
        throw new RuntimeException(e);
    }

    /**
     * 执行最终
     */
    protected void executeFinally() {

    }

}
