package ${package.Mapper};

import ${sikaPackage.Entity}.${sikaEntityBodyName}PO;
import com.sika.code.infractructure.db.mapper.BaseMapper;

/**
 * <p>
 * ${table.comment!} Mapper 接口
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
public interface ${table.mapperName} extends BaseMapper<${sikaPrimaryType}, ${sikaEntityBodyName}PO> {

}
