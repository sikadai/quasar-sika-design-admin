package ${package.Entity};


import ${sikaPackage.Repository}.${sikaEntityBodyName}Repository;
import com.sika.code.infrastructure.common.base.pojo.domain.factory.BaseFactory;

/**
 * <p>
 * ${table.comment!}工厂类
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
public interface ${entity} {

    static ${sikaEntityBodyName}Repository get${sikaEntityBodyName}Repository() {
        return BaseFactory.getBeanObj(${sikaEntityBodyName}Repository.class);
    }
}