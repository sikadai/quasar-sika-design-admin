package ${package.Service};

import ${sikaPackage.Entity}.${sikaEntityBodyName}PO;
import com.sika.code.infrastructure.common.base.repository.BaseRepository;

/**
 * <p>
 * ${table.comment!} 持久化操作类
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if kotlin>
interface ${table.serviceName} : ${superServiceClass}<${entity}>
<#else>
public interface ${table.serviceName} extends BaseRepository<${sikaPrimaryType}, ${sikaEntityBodyName}PO> {

}
</#if>
