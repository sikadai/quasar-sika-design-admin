package ${package.ServiceImpl};

import ${sikaPackage.Entity}.${sikaEntityBodyName}PO;
import ${sikaPackage.Mapper}.${table.mapperName};
import ${package.Service}.${table.serviceName};
import com.sika.code.infractructure.db.repository.impl.BaseRepositoryMyBatisPlus;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * ${table.comment!} 持久化操作实现类
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
@Repository
<#if kotlin>
open class ${table.serviceImplName} : ${superServiceImplClass}<${table.mapperName}, ${entity}>(), ${table.serviceName} {

}
<#else>
public class ${table.serviceImplName} extends BaseRepositoryMyBatisPlus<${sikaPrimaryType}, ${sikaEntityBodyName}PO, ${sikaEntityBodyName}Mapper> implements ${table.serviceName} {

}
</#if>

