package ${package.Entity};


import ${sikaPackage.DTO}.${sikaEntityBodyName}DTO;
import com.sika.code.infrastructure.common.base.pojo.domain.entity.BaseStandardEntity;

/**
 * <p>
 * ${table.comment!}领域类
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
public class ${entity} extends BaseStandardEntity<${sikaEntityBodyName}DTO> {

   @Override
   protected ${sikaEntityBodyName}DTO doExecute() {
     return null;
   }
}