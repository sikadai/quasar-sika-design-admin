package ${package.Service};

import ${sikaPackage.DTO}.${sikaEntityBodyName}DTO;
import com.sika.code.infrastructure.common.base.service.BaseService;

/**
 * <p>
 * ${table.comment!} 服务类
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
<#if kotlin>
interface ${table.serviceName} : ${superServiceClass}<${entity}>
<#else>
public interface ${table.serviceName} extends BaseService<${sikaPrimaryType}, ${sikaEntityBodyName}DTO> {

}
</#if>
