package ${package.ServiceImpl};

import ${sikaPackage.DTO}.${sikaEntityBodyName}DTO;
import ${sikaPackage.PO}.${sikaEntityBodyName}PO;
import ${sikaPackage.Repository}.${sikaEntityBodyName}Repository;
import ${package.Service}.${table.serviceName};
import com.sika.code.infrastructure.common.base.service.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * ${table.comment!} 服务实现类
 * </p>
 *
 * @author ${author}
 * @since ${date}
 */
@Service
<#if kotlin>
open class ${table.serviceImplName} : ${superServiceImplClass}<${table.mapperName}, ${entity}>(), ${table.serviceName} {

}
<#else>
public class ${table.serviceImplName} extends BaseServiceImpl<${sikaPrimaryType}, ${sikaEntityBodyName}PO, ${sikaEntityBodyName}DTO, ${sikaEntityBodyName}Repository> implements ${table.serviceName} {

}
</#if>
