package com.sika.code.infractructure.generator;

import cn.hutool.core.util.StrUtil;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

/**
 * @author daiqi
 * @create 2021-10-28 0:12
 */
@Data
@Slf4j
public class GeneratorDTO {
    private String dbUrl;
    private String dbUsername;
    private String dbPassword;
    private String tableName;
    private String author;

    private String tablePrefix;
    private String tableSuffix;
    private String infrastructureOutDir;
    private String mapperXmlOutDir;
    private String domainOutDir;
    private String interfacesOutDir;
    private String applicationOutDir;

    private String testOutDir;
    private String infrastructureParent;
    private String domainParent;
    private String interfacesParent;
    private String applicationParent;
    private String testParent;

    private Boolean fileOverride;

    public String getTablePrefix() {
        if (tablePrefix == null) {
            return StrUtil.EMPTY;
        }
        return tablePrefix;
    }

    public String getTableSuffix() {
        if (tableSuffix == null) {
            return StrUtil.EMPTY;
        }
        return tableSuffix;
    }

    public Boolean getFileOverride() {
        if (fileOverride == null) {
            return false;
        }
        return fileOverride;
    }
}
