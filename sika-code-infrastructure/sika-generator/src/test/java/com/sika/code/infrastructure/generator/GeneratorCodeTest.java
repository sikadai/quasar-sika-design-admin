package com.sika.code.infrastructure.generator;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ArrayUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import com.baomidou.mybatisplus.generator.fill.Column;
import com.baomidou.mybatisplus.generator.fill.Property;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.sika.code.infractructure.generator.GeneratorDTO;
import com.sika.code.infractructure.generator.GeneratorExecutor;
import com.sika.code.infrastructure.common.base.pojo.command.BaseCommand;
import com.sika.code.infrastructure.common.base.pojo.domain.valueobject.BaseValueObject;
import com.sika.code.infrastructure.common.base.pojo.po.BasePO;
import com.sika.code.infrastructure.common.base.pojo.query.PageQuery;
import com.sika.code.infrastructure.common.base.test.BaseTestRepository;
import lombok.extern.slf4j.Slf4j;
import org.junit.Test;

import java.util.*;

/**
 * @author daiqi
 * @create 2021-10-22 23:59
 */
@Slf4j
public class GeneratorCodeTest {
    private static Map<String, String> filePathMap = Maps.newHashMap();
    private String tablePrefix = "sika_";
    private String tableSuffix = "";
    private String infrastructureOutDir = "E:\\project\\my\\quasar-sika-design-admin-gitee\\quasar-sika-design-admin\\sika-code-infrastructure\\sika-generator\\src\\main\\java\\";
    private String infrastructureXmlOutDir = "E:\\project\\my\\quasar-sika-design-admin-gitee\\quasar-sika-design-admin\\sika-code-infrastructure\\sika-generator\\src\\main\\resources\\";
    private String domainOutDir = "E:\\project\\my\\quasar-sika-design-admin-gitee\\quasar-sika-design-admin\\sika-code-infrastructure\\sika-generator\\src\\main\\java\\";
    private String interfacesOutDir = "E:\\project\\my\\quasar-sika-design-admin-gitee\\quasar-sika-design-admin\\sika-code-infrastructure\\sika-generator\\src\\main\\java\\";
    private String applicationOutDir = "E:\\project\\my\\quasar-sika-design-admin-gitee\\quasar-sika-design-admin\\sika-code-infrastructure\\sika-generator\\src\\main\\java\\";

    private String testOutDir = "E:\\project\\my\\quasar-sika-design-admin-gitee\\quasar-sika-design-admin\\sika-code-infrastructure\\sika-generator\\src\\test\\java\\";

    private static String infrastructureParent = "com.sika.code.infrastructure.generator.business";
    private static String domainParent = "com.sika.code.domain.generator.business";
    private static String interfacesParent = "com.sika.code.interfaces.generator.business";
    private static String applicationParent = "com.sika.code.application.generator.business";
    private static String testParent = "com.sika.code.generator.business";


    private List<TemplateType> getDisableTypes(List<TemplateType> needGenerateTemplateTypes) {
        if (CollUtil.isEmpty(needGenerateTemplateTypes)) {
            return CollUtil.newArrayList(TemplateType.values());
        }
        List<TemplateType> templateTypesForRet = Lists.newArrayList();
        for (TemplateType templateType : TemplateType.values()) {
            if (needGenerateTemplateTypes.contains(templateType)) {
                continue;
            }
            templateTypesForRet.add(templateType);
        }
        return templateTypesForRet;
    }

    /**
     * 数据源配置
     */
    private final DataSourceConfig DATA_SOURCE_CONFIG = new DataSourceConfig
            .Builder("jdbc:mysql://121.89.202.68:3306/sika-design-admin?useUnicode=true&characterEncoding=UTF-8&serverTimezone=UTC", "root", "SikaDesignAdmin20201225")
            .build();

    /**
     * 策略配置
     */
    private StrategyConfig.Builder strategyConfig() {
        return new StrategyConfig.Builder().addInclude("sika_user"); // 设置需要生成的表名
    }

    /**
     * 全局配置
     */
    private GlobalConfig.Builder globalConfig() {
        return new GlobalConfig.Builder()
                .author("sikadai")
                .disableOpenDir()
                .fileOverride()
                .commentDate("yyyy-MM-dd HH:mm:ss");
    }

    /**
     * 包配置
     */
    private PackageConfig.Builder packageConfig() {
        return new PackageConfig.Builder();
    }

    /**
     * 模板配置
     */
    private TemplateConfig.Builder templateConfig() {
        return new TemplateConfig.Builder().disable();
    }

    /**
     * 注入配置
     */
    private InjectionConfig.Builder injectionConfig() {
        // 测试自定义输出文件之前注入操作，该操作再执行生成代码前 debug 查看
        return new InjectionConfig.Builder().beforeOutputFile((tableInfo, objectMap) -> {
            System.out.println("tableInfo: " + tableInfo.getEntityName() + " objectMap: " + objectMap.toString());
        });
    }

    @Test
    public void testGenerateByExecutor() {
        GeneratorDTO generatorDTO = new GeneratorDTO();
        generatorDTO.setDbUrl("jdbc:mysql://121.89.202.68:3306/sika-design-admin?useUnicode=true&characterEncoding=UTF-8&serverTimezone=UTC");
        generatorDTO.setDbUsername("root");
        generatorDTO.setDbPassword("SikaDesignAdmin20201225");
        generatorDTO.setTableName("sika_user");
        generatorDTO.setAuthor("sikadai");

        generatorDTO.setTablePrefix("sika_");
        generatorDTO.setTableSuffix("");
        String projectAbsolutePath = "E:\\project\\sika-design-gitee\\quasar-sika-design-admin\\sika-code-infrastructure\\sika-generator\\";
        generatorDTO.setInfrastructureOutDir(projectAbsolutePath + "src\\main\\java\\");
        generatorDTO.setMapperXmlOutDir(projectAbsolutePath + "src\\main\\resources\\");
        generatorDTO.setDomainOutDir(projectAbsolutePath + "src\\main\\java\\");
        generatorDTO.setInterfacesOutDir(projectAbsolutePath + "src\\main\\java\\");
        generatorDTO.setApplicationOutDir(projectAbsolutePath + "src\\main\\java\\");

        generatorDTO.setTestOutDir(projectAbsolutePath + "src\\test\\java\\");
        generatorDTO.setInfrastructureParent("com.sika.code.infrastructure.generator.business");
        generatorDTO.setDomainParent("com.sika.code.domain.generator.business");
        generatorDTO.setInterfacesParent("com.sika.code.interfaces.generator.business");
        generatorDTO.setApplicationParent("com.sika.code.application.generator.business");
        generatorDTO.setTestParent("com.sika.code.generator.business");
        GeneratorExecutor executor = new GeneratorExecutor(generatorDTO);
        executor.execute();
    }

    /**
     * 简单生成
     */
    @Test
    public void testSimple() {
        AutoGenerator generator = new AutoGenerator(DATA_SOURCE_CONFIG);
        StrategyConfig strategyConfig = strategyConfig().addTablePrefix(tablePrefix)
                .entityBuilder()
                .enableLombok()
                .formatFileName("%sPO")
                .disableSerialVersionUID()
                .superClass("com.sika.code.standard.base.pojo.entity.BaseStandardEntity")
                .addIgnoreColumns("id", "create_by", "update_by", "create_date", "update_date",
                        "version", "available", "is_deleted", "remark").build(); // 基于数据库字段
        GlobalConfig globalConfig = globalConfig().outputDir(infrastructureOutDir).build();
        generator.global(globalConfig);

        String moduleName = getEntityName(strategyConfig.getInclude().iterator().next()).toLowerCase(Locale.ROOT);
        PackageConfig packageConfig = packageConfig().parent(infrastructureParent + ".pojo" + "." + moduleName).entity("po").build();
        generator.packageInfo(packageConfig);

        generator.strategy(strategyConfig);
        generator.template(templateConfig()
                .entity("/template/entity.java")
                .disable(TemplateType.CONTROLLER, TemplateType.MAPPER, TemplateType.XML, TemplateType.SERVICE)
                .build());

        Map<String, Object> map = new HashMap<>();
        List<String> extendPkg = new ArrayList<>();
//        extendPkg.add("java.io.Serializable");
        map.put("extendPkg", extendPkg);

        Map<String, String> customFile = new HashMap<>();

        generator.injection(injectionConfig().customMap(map).customFile(customFile).build());
        generator.execute(new CustomerFreemarkerTemplateEngine());
    }

    @Test
    public void generateCode() {
        generateInfrastructure();
        generateDomain();
        generateApplication();
        generateInterfaces();
    }

    @Test
    public void generateInfrastructure() {
        List<TemplateType> templateTypes = getDisableTypes(Lists.newArrayList(TemplateType.ENTITY));
        String moduleSubName = ".pojo";
        generatePo(moduleSubName, templateTypes);
        generateDot(moduleSubName, templateTypes);
        generateQuery(moduleSubName, templateTypes);
        generateValueObject(moduleSubName, templateTypes);
    }

    @Test
    public void generateDomain() {
        generateMapper();
        generateXml();
        generateRepository();
        generateEntity();
        generateFactory();
    }

    private void generateApplication() {
        generateService();
    }

    private void generateInterfaces() {
        generateController();
        generateRpc();
    }

    private void generateDot(String moduleSubName, List<TemplateType> templateTypes) {
        generateCore("", "DTO", moduleSubName, templateTypes, "/template/dto", infrastructureOutDir, infrastructureParent, BaseCommand.class.getName());
    }

    public void generatePo(String moduleSubName, List<TemplateType> templateTypes) {
        generateCore("", "PO", moduleSubName, templateTypes, "/template/po", infrastructureOutDir, infrastructureParent, BasePO.class.getName());
    }


    private void generateQuery(String moduleSubName, List<TemplateType> templateTypes) {
        generateCore("", "Query", moduleSubName, templateTypes, "/template/query", infrastructureOutDir, infrastructureParent, PageQuery.class.getName());
    }

    private void generateValueObject(String moduleSubName, List<TemplateType> templateTypes) {
        generateCore("", "ValueObject", moduleSubName, templateTypes, "/template/valueObject", infrastructureOutDir, infrastructureParent, BaseValueObject.class.getName());
    }

    @Test
    public void generateMapper() {
        List<TemplateType> templateTypes = getDisableTypes(Lists.newArrayList(TemplateType.MAPPER));
        String moduleSubName = ".db";
        generateCore("", "Mapper", moduleSubName, templateTypes, "/template/mapper", domainOutDir, domainParent, null);
    }

    @Test
    public void generateXml() {
        List<TemplateType> templateTypes = getDisableTypes(Lists.newArrayList(TemplateType.XML));
        AutoGenerator generator = new AutoGenerator(DATA_SOURCE_CONFIG);
        StrategyConfig strategyConfig = strategyConfig().addTablePrefix(tablePrefix)
                .mapperBuilder().enableBaseResultMap().enableBaseColumnList().build(); // 基于数据库字段
        GlobalConfig globalConfig = globalConfig().outputDir(infrastructureXmlOutDir).build();
        generator.global(globalConfig);
        Map<OutputFile, String> pathInfo = Maps.newHashMap();
        pathInfo.put(OutputFile.mapperXml, infrastructureXmlOutDir + "/mapper");
        String entityBodyName = getEntityName(strategyConfig.getInclude().iterator().next());
        String moduleName = entityBodyName.toLowerCase(Locale.ROOT);
        log.info("moduleName:{}", moduleName);
        PackageConfig packageConfig = packageConfig()
                .parent(infrastructureParent + "." + moduleName)
                .entity("pojo.po")
                .mapper("db.mapper")
                .pathInfo(pathInfo)
                .xml("mapper").build();
        generator.packageInfo(packageConfig);

        generator.strategy(strategyConfig);
        generator.template(templateConfig()
                .mapperXml("/template/mapper.xml")
                .disable(ArrayUtil.toArray(templateTypes, TemplateType.class))
                .build());

        Map<String, Object> map = new HashMap<>();
        List<String> extendPkg = new ArrayList<>();
//        extendPkg.add("java.io.Serializable");
        map.put("extendPkg", extendPkg);
        Map<String, String> sikaPackage = Maps.newHashMap();
        sikaPackage.putAll(packageConfig.getPackageInfo());
        sikaPackage.put(ConstVal.MAPPER, getMapperPackage(moduleName));
        sikaPackage.put("Query", getQueryPackage(moduleName));
        map.put("sikaPackage", sikaPackage);
        map.put("sikaEntityBodyName", entityBodyName);

        Map<String, String> customFile = new HashMap<>();
        generator.injection(injectionConfig().customMap(map).customFile(customFile).build());
        generator.execute(new CustomerFreemarkerTemplateEngine());
    }

    private String getMapperPackage(String moduleName) {
        return domainParent + "." + moduleName + ".db.mapper";
    }

    private String getQueryPackage(String moduleName) {
        return infrastructureParent + "." + moduleName + ".pojo.query";
    }

    private String getPoPackage(String moduleName) {
        return infrastructureParent + "." + moduleName + ".pojo.po";
    }

    private String getDtoPackage(String moduleName) {
        return infrastructureParent + "." + moduleName + ".pojo.dto";
    }

    private String getRepositoryPackage(String moduleName) {
        return domainParent + "." + moduleName + ".db.repository";
    }

    private String getServicePackage(String moduleName) {
        return applicationParent + "." + moduleName + ".service";
    }

    @Test
    public void generateRepository() {
        List<TemplateType> templateTypes = getDisableTypes(Lists.newArrayList(TemplateType.SERVICE, TemplateType.SERVICEIMPL));
        String moduleSubName = ".db";
        generateCore("", "Repository", moduleSubName, templateTypes, "/template/repository", domainOutDir, domainParent, null);
    }

    @Test
    public void generateEntity() {
        List<TemplateType> templateTypes = getDisableTypes(Lists.newArrayList(TemplateType.ENTITY));
        String moduleSubName = ".domain";
        generateCore("", "entity", moduleSubName, templateTypes, "/template/entity", domainOutDir, domainParent, null);
    }

    @Test
    public void generateFactory() {
        List<TemplateType> templateTypes = getDisableTypes(Lists.newArrayList(TemplateType.SERVICE));
        String moduleSubName = ".domain";
        generateCore("", "factory", moduleSubName, templateTypes, "/template/factory", domainOutDir, domainParent, null);
    }

    @Test
    public void generateService() {
        List<TemplateType> templateTypes = getDisableTypes(Lists.newArrayList(TemplateType.SERVICE, TemplateType.SERVICEIMPL));
        String moduleSubName = "";
        generateCore("", "Service", moduleSubName, templateTypes, "/template/service", applicationOutDir, applicationParent, null);
    }


    @Test
    public void generateController() {
        List<TemplateType> templateTypes = getDisableTypes(Lists.newArrayList(TemplateType.CONTROLLER));
        String moduleSubName = "";
        generateCore("", "Controller", moduleSubName, templateTypes, "/template/controller", interfacesOutDir, interfacesParent, null);
    }

    private void generateRpc() {
        List<TemplateType> templateTypes = getDisableTypes(Lists.newArrayList(TemplateType.CONTROLLER));
        String moduleSubName = "";
        generateCore("", "Rpc", moduleSubName, templateTypes, "/template/controller", interfacesOutDir, interfacesParent, null);
    }

    private void generateTest() {
        generateTestRepository();
        generateTestEntity();
        generateTestService();
        generateTestController();
    }

    @Test
    public void generateTestRepository() {
        List<TemplateType> templateTypes = getDisableTypes(Lists.newArrayList(TemplateType.ENTITY));
        String moduleSubName = "";
        generateCore("Test", "Repository", moduleSubName, templateTypes, "/template/repository.test", testOutDir, testParent, BaseTestRepository.class.getName());
    }

    @Test
    public void generateTestController() {
        List<TemplateType> templateTypes = getDisableTypes(Lists.newArrayList(TemplateType.ENTITY));
        String moduleSubName = "";
        generateCore("Test", "Controller", moduleSubName, templateTypes, "/template/controller.test", testOutDir, testParent, BaseTestRepository.class.getName());
    }

    @Test
    public void generateTestService() {
        List<TemplateType> templateTypes = getDisableTypes(Lists.newArrayList(TemplateType.ENTITY));
        String moduleSubName = "";
        generateCore("Test", "Service", moduleSubName, templateTypes, "/template/service.test", testOutDir, testParent, BaseTestRepository.class.getName());
    }

    @Test
    public void generateTestEntity() {
        List<TemplateType> templateTypes = getDisableTypes(Lists.newArrayList(TemplateType.ENTITY));
        String moduleSubName = "";
        generateCore("Test", "Entity", moduleSubName, templateTypes, "/template/entity.test", testOutDir, testParent, BaseTestRepository.class.getName());
    }

    private void generateCore(String objPrefix, String objSuffix, String moduleSubName, List<TemplateType> templateTypes, String templatePath, String outDir, String parent, String superClass) {
        String templatePathTemp = templatePath;
        if (!templatePath.contains(".xml")) {
            templatePathTemp = templatePath + ".java";
        }
        AutoGenerator generator = new AutoGenerator(DATA_SOURCE_CONFIG);
        StrategyConfig strategyConfig = strategyConfig().addTablePrefix(tablePrefix)
                .controllerBuilder()
                .formatFileName("%s" + objSuffix)
                .enableRestStyle()
                .serviceBuilder()
                .formatServiceFileName("%s" + objSuffix)
                .formatServiceImplFileName("%s" + objSuffix + "Impl")
                .entityBuilder()
                .enableLombok()
                .formatFileName(objPrefix + "%s" + objSuffix)
                .disableSerialVersionUID()
                .superClass(superClass)
                .addIgnoreColumns("id", "create_by", "update_by", "create_date", "update_date",
                        "version", "available", "is_deleted", "remark").build(); // 基于数据库字段
        GlobalConfig globalConfig = globalConfig().outputDir(outDir).build();
        generator.global(globalConfig);

        String entityBodyName = getEntityName(strategyConfig.getInclude().iterator().next());
        String moduleName = entityBodyName.toLowerCase(Locale.ROOT);
        PackageConfig packageConfig = packageConfig().parent(parent + "." + moduleName + moduleSubName)
                .entity(objSuffix.toLowerCase())
                .service(objSuffix.toLowerCase())
                .serviceImpl(objSuffix.toLowerCase() + ".impl")
                .controller(objSuffix.toLowerCase())
                .mapper(objSuffix.toLowerCase())
                .build();
        generator.packageInfo(packageConfig);

        generator.strategy(strategyConfig);
        generator.template(templateConfig()
                .entity(templatePathTemp)
                .mapper(templatePathTemp)
                .service(templatePathTemp)
                .serviceImpl(templatePath + "Impl.java")
                .controller(templatePathTemp)
                .mapperXml(templatePathTemp)
                .disable(ArrayUtil.toArray(templateTypes, TemplateType.class))
                .build());

        Map<String, Object> map = new HashMap<>();
        List<String> extendPkg = new ArrayList<>();
        Map<String, String> sikaPackage = Maps.newHashMap();
        sikaPackage.putAll(packageConfig.getPackageInfo());
        sikaPackage.put(ConstVal.MAPPER, getMapperPackage(moduleName));
        sikaPackage.put("PO", getPoPackage(moduleName));
        sikaPackage.put("Query", getQueryPackage(moduleName));
        sikaPackage.put("Entity", getPoPackage(moduleName));
        sikaPackage.put("DTO", getDtoPackage(moduleName));
        sikaPackage.put("Repository", getRepositoryPackage(moduleName));
        sikaPackage.put("Service", getServicePackage(moduleName));
        map.put("sikaPackage", sikaPackage);
        map.put("extendPkg", extendPkg);
        map.put("sikaPrimaryType", "Long");
        map.put("sikaEntityBodyName", entityBodyName);
        Map<String, String> customFile = new HashMap<>();
        generator.injection(injectionConfig().customMap(map).customFile(customFile).build());
        generator.execute(new CustomerFreemarkerTemplateEngine());
    }

    private String getEntityName(String str) {
        if (StrUtil.isNotBlank(tablePrefix)) {
            int prefixIndex = str.indexOf(tablePrefix);
            if (prefixIndex >= 0) {
                str = str.substring(tablePrefix.length());
            }
        }
        if (StrUtil.isNotBlank(tableSuffix)) {
            int afterIndex = str.lastIndexOf(tableSuffix);
            if (afterIndex >= 0) {
                str = str.substring(0, afterIndex);
            }
        }
        return firstToUpperCase(str);
    }

    private String firstToUpperCase(String str) {
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }


    private String buildFullPath(GlobalConfig globalConfig, PackageConfig packageConfig, String moduleName) {
        return globalConfig.getOutputDir() + packageConfig.getParent().replace(".", "\\") + "\\" + moduleName;
    }

    public static class CustomerFreemarkerTemplateEngine extends FreemarkerTemplateEngine {

    }

    /**
     * 过滤表前缀（后缀同理，支持多个）
     * result: t_simple -> simple
     */
    @Test
    public void testTablePrefix() {
        AutoGenerator generator = new AutoGenerator(DATA_SOURCE_CONFIG);
        generator.strategy(strategyConfig().addTablePrefix("t_", "c_").build());
        generator.global(globalConfig().build());
        generator.execute();
    }

    /**
     * 过滤字段后缀（前缀同理，支持多个）
     * result: deleted_flag -> deleted
     */
    @Test
    public void testFieldSuffix() {
        AutoGenerator generator = new AutoGenerator(DATA_SOURCE_CONFIG);
        generator.strategy(strategyConfig().addFieldSuffix("_flag").build());
        generator.global(globalConfig().build());
        generator.execute();
    }

    /**
     * 乐观锁字段设置
     * result: 新增@Version注解
     * 填充字段设置
     * result: 新增@TableField(value = "xxx", fill = FieldFill.xxx)注解
     */
    @Test
    public void testVersionAndFill() {
        AutoGenerator generator = new AutoGenerator(DATA_SOURCE_CONFIG);
        generator.strategy(strategyConfig().entityBuilder()
                .versionColumnName("version") // 基于数据库字段
                .versionPropertyName("version")// 基于模型属性
                .addTableFills(new Column("create_time", FieldFill.INSERT))    //基于数据库字段填充
                .addTableFills(new Property("updateTime", FieldFill.INSERT_UPDATE))    //基于模型属性填充
                .build());
        generator.global(globalConfig().build());
        generator.execute();
    }

    /**
     * 逻辑删除字段设置
     * result: 新增@TableLogic注解
     * 忽略字段设置
     * result: 不生成
     */
    @Test
    public void testLogicDeleteAndIgnoreColumn() {
        AutoGenerator generator = new AutoGenerator(DATA_SOURCE_CONFIG);
        generator.strategy(strategyConfig().entityBuilder()
                .logicDeleteColumnName("deleted") // 基于数据库字段
                .logicDeletePropertyName("deleteFlag")// 基于模型属性
                .addIgnoreColumns("age") // 基于数据库字段
                .build());
        generator.global(globalConfig().build());
        generator.execute();
    }

    /**
     * 自定义模板生成的文件名称
     * result: TSimple -> TSimpleEntity
     */
    @Test
    public void testCustomTemplateName() {
        AutoGenerator generator = new AutoGenerator(DATA_SOURCE_CONFIG);
        generator.strategy(strategyConfig()
                .entityBuilder().formatFileName("%sEntity")
                .mapperBuilder().formatMapperFileName("%sDao").formatXmlFileName("%sXml")
                .controllerBuilder().formatFileName("%sAction")
                .serviceBuilder().formatServiceFileName("%sService").formatServiceImplFileName("%sServiceImp")
                .build());
        generator.global(globalConfig().build());
        generator.execute();
    }

    /**
     * 自定义模板生成的文件路径
     *
     * @see OutputFile
     */
    @Test
    public void testCustomTemplatePath() {
        // 设置自定义路径
        Map<OutputFile, String> pathInfo = new HashMap<>();
        pathInfo.put(OutputFile.mapperXml, "D://");
        pathInfo.put(OutputFile.entity, "D://entity//");
        AutoGenerator generator = new AutoGenerator(DATA_SOURCE_CONFIG);
        generator.strategy(strategyConfig().build());
        generator.packageInfo(packageConfig().pathInfo(pathInfo).build());
        generator.global(globalConfig().build());
        generator.execute();
    }

    /**
     * 自定义模板
     */
    @Test
    public void testCustomTemplate() {
        AutoGenerator generator = new AutoGenerator(DATA_SOURCE_CONFIG);
        generator.strategy(strategyConfig().build());
        generator.template(templateConfig()
                .entity("/templates/entity1.java.ftl")
                .build());
        generator.global(globalConfig().build());
        generator.execute();
    }

    /**
     * 自定义注入属性
     */
    @Test
    public void testCustomMap() {
        // 设置自定义属性
        Map<String, Object> map = new HashMap<>();
        map.put("abc", 123);
        AutoGenerator generator = new AutoGenerator(DATA_SOURCE_CONFIG);
        generator.strategy(strategyConfig().build());
        generator.template(templateConfig()
                .entity("/templates/entity1.java")
                .build());
        generator.injection(injectionConfig().customMap(map).build());
        generator.global(globalConfig().build());
        generator.execute();
    }

    /**
     * 自定义文件
     * key为文件名称，value为文件路径
     * 输出目录为 other
     */
    @Test
    public void testCustomFile() {
        // 设置自定义输出文件
        Map<String, String> customFile = new HashMap<>();
        customFile.put("test.txt", "/templates/test.vm");
        AutoGenerator generator = new AutoGenerator(DATA_SOURCE_CONFIG);
        generator.strategy(strategyConfig().build());
        generator.injection(injectionConfig().customFile(customFile).build());
        generator.global(globalConfig().build());
        generator.execute();
    }
}
