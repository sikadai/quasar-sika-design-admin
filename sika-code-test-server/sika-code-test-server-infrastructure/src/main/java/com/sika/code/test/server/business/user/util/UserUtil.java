package com.sika.code.test.server.business.user.util;

import cn.hutool.core.lang.Assert;
import cn.hutool.crypto.SecureUtil;

public class UserUtil {
    /**
     * 对密码进行加密
     */
    public static String encryptPassword(String clientPwd) {
        Assert.notBlank(clientPwd, "密码为空");
        return SecureUtil.md5(clientPwd);
    }
}
