package com.sika.code.test.server.business.auth.constant;

import com.sika.code.infrastructure.common.base.constant.BaseTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum LoginCodeEnum implements BaseTypeEnum<Integer> {
    MAIL_CODE(1, "LOGIN:CODE:MAIL:" + LoginCodeEnum.class.getName(), "邮箱验证码"),
    PHONE_SMS_CODE(2, "LOGIN:CODE:PHONE:CODE:" + LoginCodeEnum.class.getName(), "手机号短信验证码"),
    ;
    private Integer type;
    private String codePrefix;
    private String desc;

    public String buildFullKey(String key) {
        return this.getCodePrefix() + ":" + key;
    }
}
