package com.sika.code.test.server.business.testuser.pojo.po;

import com.baomidou.mybatisplus.annotation.TableName;
import com.sika.code.infrastructure.common.base.pojo.po.BasePO;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 测试用户表
 * </p>
 *
 * @author daiqi
 * @since 2021-07-03 15:44:03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@TableName("test_user")
public class TestUserPO extends BasePO<Long> {

    private static final long serialVersionUID = 1L;

    /**
     * 用户id
     */
    private Long userId;

    /**
     * 用户名称
     */
    private String userName;

}
