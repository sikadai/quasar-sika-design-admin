package com.sika.code.test.server.business.thirdoauthuser.pojo.valueobject;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 第三方授权用户表 值对象类
 * </p>
 *
 * @author sikadai
 * @since 2021-11-11 00:22:30
 */
@Getter
@Setter
public class ThirdOauthUserValueObject {
    private static final long serialVersionUID = 1L;
}
