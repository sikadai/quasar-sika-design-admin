package com.sika.code.test.server.business.user.pojo.valueobject;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 用户表 值对象类
 * </p>
 *
 * @author sikadai
 * @since 2021-10-31 17:45:03
 */
@Getter
@Setter
public class UserValueObject {
    private static final long serialVersionUID = 1L;
}
