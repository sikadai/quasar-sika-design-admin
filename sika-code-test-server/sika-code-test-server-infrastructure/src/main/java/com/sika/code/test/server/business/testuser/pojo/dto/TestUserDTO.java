package com.sika.code.test.server.business.testuser.pojo.dto;

import com.sika.code.infrastructure.common.base.pojo.dto.BaseDTO;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 测试用户表
 * </p>
 *
 * @author daiqi
 * @since 2021-07-03 15:44:00
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class TestUserDTO extends BaseDTO<Long> {

    private static final long serialVersionUID = 1L;

    /**
     * 数据表id
     */
    private Long testUserId;
    /**
     * 用户id
     */
    private Long userId;
    /**
     * 用户名称
     */
    private String userName;

}
