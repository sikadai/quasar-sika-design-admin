package com.sika.code.test.server.business.thirdoauthuser.pojo.converter;
import com.sika.code.infrastructure.common.base.convert.BaseConverter;
import com.sika.code.test.server.business.thirdoauthuser.pojo.dto.ThirdOauthUserDTO;
import com.sika.code.test.server.business.thirdoauthuser.pojo.po.ThirdOauthUserPO;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import java.util.List;

/**
 * <p>
 * 第三方授权用户表 转化类
 * </p>
 *
 * @author sikadai
 * @since 2021-11-16 00:40:32
 */
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ThirdOauthUserConverter extends BaseConverter {
    /**
     * <p>
     * 转化类的单例对象
     * </p>
     */
    ThirdOauthUserConverter INSTANCE = Mappers.getMapper(ThirdOauthUserConverter.class);

    /**
     * 将DTO对象转化为PO对象
     */
    ThirdOauthUserPO convertToPo(ThirdOauthUserDTO dto);

    /**
     * 将PO对象转化为DTO对象
     */
    ThirdOauthUserDTO convertToDto(ThirdOauthUserPO po);

    /**
     * 将DTO对象列表转化为PO对象列表
     */
    List<ThirdOauthUserPO> convertToPos(List<ThirdOauthUserDTO> dtos);

    /**
     * 将PO对象列表转化为DTO对象列表
     */
    List<ThirdOauthUserDTO> convertToDtos(List<ThirdOauthUserPO> pos);

}
