package com.sika.code.test.server.business.user.converter;

import cn.hutool.extra.cglib.CglibUtil;
import com.sika.code.infrastructure.common.base.convert.BaseConverter;
import com.sika.code.infrastructure.common.base.util.JSONUtil;
import com.sika.code.test.server.business.auth.pojo.dto.LoginUserDTO;
import com.sika.code.test.server.business.user.pojo.dto.UserDTO;
import com.sika.code.test.server.business.user.pojo.po.UserPO;
import com.sika.code.test.server.business.user.pojo.query.UserQuery;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;
import org.mapstruct.factory.Mappers;
import org.springframework.beans.BeanUtils;

import java.util.List;

/**
 * <p>
 * 用户转换类
 * </p>
 *
 * @author daiqi
 * @date 2021/11/10 23:53
 */
@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface UserConverter extends BaseConverter {
    /**
     * <p>
     * 转化类的单例对象
     * </p>
     */
    UserConverter INSTANCE = Mappers.getMapper(UserConverter.class);

    UserDTO convertToDto(UserPO userPO);

    LoginUserDTO convertToLoginUserDto(UserPO userPO);

    UserDTO convertToDto(UserQuery userQuery);

    List<UserDTO> convertToPos(List<UserQuery> userQuerys);

    static void convertToDtosUseSpring(UserQuery userQuery) {
        for (long i = 0; i < userQuery.getId(); ++i) {
            UserPO userPO = new UserPO();
            BeanUtils.copyProperties(userQuery, userPO);
        }
    }

    static void convertToDtosUseJon(UserQuery userQuery) {
        for (long i = 0; i < userQuery.getId(); ++i) {
            JSONUtil.parseObject(userQuery, UserPO.class);
        }
    }

    static void convertToDtosUseCglibUtil(UserQuery userQuery) {
        for (long i = 0; i < userQuery.getId(); ++i) {
            CglibUtil.copy(userQuery, UserPO.class);
        }
    }

    static void convertToDtosUseMapStruct(UserQuery userQuery) {
        for (long i = 0; i < userQuery.getId(); ++i) {
            UserConverter.INSTANCE.convertToDto(userQuery);
        }
    }

    static void convertToDtosUseOrginar(UserQuery userQuery) {
        for (long i = 0; i < userQuery.getId(); ++i) {
            UserPO userPO = new UserPO();
            userPO.setId(userQuery.getId());
            userPO.setAddress(userQuery.getAddress());
            userPO.setUsername(userQuery.getUsername());
            userPO.setEmail(userQuery.getEmail());
            userPO.setPhone(userQuery.getPhone());
        }
    }

}
