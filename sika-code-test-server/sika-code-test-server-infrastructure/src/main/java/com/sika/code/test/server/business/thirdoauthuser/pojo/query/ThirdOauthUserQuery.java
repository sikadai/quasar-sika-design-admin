package com.sika.code.test.server.business.thirdoauthuser.pojo.query;

import com.sika.code.infrastructure.common.base.pojo.query.PageQuery;
import lombok.Getter;
import lombok.Setter;
import java.util.List;

/**
 * <p>
 * 第三方授权用户表 查询类
 * </p>
 *
 * @author sikadai
 * @since 2021-11-11 00:22:30
 */
@Getter
@Setter
public class ThirdOauthUserQuery extends PageQuery<Long> {
    private static final long serialVersionUID = 1L;
    /**
     * 创建人标识
     */
    private String createBy;
    /**
     * 最后更新人标识
     */
    private String updateBy;
    /**
     * 第三方uuid
     */
    private String uuid;
    /**
     * 第三方用户名称
     */
    private String username;
    /**
     * 第三方昵称
     */
    private String nickname;
    /**
     * 头像链接
     */
    private String avatar;
    /**
     * 博客
     */
    private String blog;
    /**
     * 公司或者组织
     */
    private String company;
    /**
     * 所在地点
     */
    private String location;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 性别
     */
    private String gender;
    /**
     * 来源【gitee|gethub】等等
     */
    private String source;
    /**
     * token
     */
    private String token;
    /**
     * rawUserInfo
     */
    private String rawUserInfo;
    /**
     * 关联用户id【为0表示没有关联】
     */
    private Long userId;
    /**
     * 第三方授权登录的state
     */
    private String state;
    private List<Long> ids;
}
