package com.sika.code.test.server.business.auth.pojo.request;

import lombok.Data;

/**
 * <p>
 * 用户注册请求值对象
 * </p>
 *
 * @author daiqi
 * @date 2021/11/8 23:51
 */
@Data
public class RegisterRequest extends BaseAuthRequest{
    private String username;
    private String password;
    private Integer email;
    private Integer phone;

    private Integer registerType;
}
