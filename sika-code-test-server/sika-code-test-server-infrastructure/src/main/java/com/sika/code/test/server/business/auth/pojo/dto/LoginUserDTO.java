package com.sika.code.test.server.business.auth.pojo.dto;

import com.sika.code.infrastructure.common.base.pojo.dto.BaseDTO;
import lombok.Data;

/**
 * @author daiqi
 * @create 2021-11-16 0:59
 */
@Data
public class LoginUserDTO extends BaseDTO<Long> {
    private Long id;
    private String username;
    private String password;
}
