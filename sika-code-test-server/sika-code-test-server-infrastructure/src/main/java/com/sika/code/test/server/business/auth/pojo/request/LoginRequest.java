package com.sika.code.test.server.business.auth.pojo.request;

import lombok.Data;

/**
 * <p>
 * 用户登录值对象
 * </p>
 *
 * @author daiqi
 * @date 2021/11/8 23:51
 */
@Data
public class LoginRequest extends BaseAuthRequest {
    private String loginCredentials;
    private String loginKey;
    private Integer loginType;
}
