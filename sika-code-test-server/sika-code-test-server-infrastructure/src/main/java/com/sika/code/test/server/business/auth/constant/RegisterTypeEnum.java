package com.sika.code.test.server.business.auth.constant;

import com.sika.code.infrastructure.common.base.constant.BaseTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum RegisterTypeEnum implements BaseTypeEnum<Integer> {
    /**
     * 用户注册类型枚举
     */
    ORDINARY(10, "普通注册"),
    THIRD_PARTY_AUTHORIZATION(20, "第三方授权注册"),
    ;
    private final Integer type;
    private final String desc;

    public static boolean isOrdinary(Integer type) {
        return ORDINARY.getType().equals(type);
    }

    public static boolean isThirdPartyAuthorization(Integer type) {
        return THIRD_PARTY_AUTHORIZATION.getType().equals(type);
    }

}
