package com.sika.code.test.server.business.auth.pojo.request;

import lombok.Data;

/**
 * @author daiqi
 * @create 2021-11-19 0:51
 */
@Data
public class BaseAuthRequest {
    private Integer authUserType;
}
