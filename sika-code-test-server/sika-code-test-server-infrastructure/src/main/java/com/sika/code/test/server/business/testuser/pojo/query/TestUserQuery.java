package com.sika.code.test.server.business.testuser.pojo.query;

import com.sika.code.infrastructure.common.base.pojo.query.PageQuery;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Set;

/**
 * <p>
 * 测试用户表 查询类
 * </p>
 *
 * @author daiqi
 * @since 2021-07-03 15:44:01
 */
@Data
@EqualsAndHashCode(callSuper = true)
public class TestUserQuery extends PageQuery<Long> {

    private static final long serialVersionUID = 1L;
    /**
     * 数据表id
     */
    protected Long testUserId;
    /**
     * 用户id
     */
    protected Long userId;
    /**
     * 用户名称
     */
    protected String userName;
    /**
     * id列表
     */
    protected Set<Long> ids;


}

