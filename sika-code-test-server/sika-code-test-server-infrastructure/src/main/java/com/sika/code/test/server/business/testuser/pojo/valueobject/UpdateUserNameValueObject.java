package com.sika.code.test.server.business.testuser.pojo.valueobject;

import com.sika.code.infrastructure.common.base.pojo.domain.valueobject.BaseValueObject;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * @author daiqi
 * @create 2021-10-20 0:51
 */
@EqualsAndHashCode(callSuper = true)
@Data
public class UpdateUserNameValueObject extends BaseValueObject {
    private Long id;
    private Long userId;
    private String userName;
}
