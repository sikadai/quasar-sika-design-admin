package com.sika.code.test.server.business.auth.constant;

import com.sika.code.infrastructure.common.base.constant.BaseTypeEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum LoginTypeEnum implements BaseTypeEnum<Integer> {
    /**
     * 用户登录类型枚举
     */
    USERNAME_PWD(10, "用户名密码登录"),
    PHONE_SMS_CODE(20, "手机号短信验证码登录"),
    MAIL_CODE(30, "邮箱验证码登录"),
    THIRD_PARTY_AUTHORIZATION(40, "第三方授权登录"),
    ;
    private final Integer type;
    private final String desc;

    public static boolean isUsernamePwd(Integer type) {
        return USERNAME_PWD.getType().equals(type);
    }

    public static boolean isPhoneSmsCode(Integer type) {
        return PHONE_SMS_CODE.getType().equals(type);
    }

    public static boolean isMailCode(Integer type) {
        return MAIL_CODE.getType().equals(type);
    }
    public static boolean isThirdPartyAuthorization(Integer type) {
        return THIRD_PARTY_AUTHORIZATION.getType().equals(type);
    }
}
