package com.sika.code.test.server;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;


/**
 * 启动类
 * <p>
 * exclude = {DataSourceAutoConfiguration.class}非常重要，如果没有关闭，在启动程序时会发生循环依赖的错误
 *
 * @author daiqi
 * @create 2019-05-10 21:16
 */
@SpringBootApplication(scanBasePackages = {"com.sika.code", "com.sika.code.test.server", "cn.hutool.extra.spring"}, exclude = {DataSourceAutoConfiguration.class})
@MapperScan({"com.sika.code.test.server.**.mapper"})
public class SikaCodeTestServerApplication {
    public static void main(String[] args) {
        SpringApplication.run(SikaCodeTestServerApplication.class, args);
    }
}
