package com.sika.code.test.server.business.auth.controller;

import cn.dev33.satoken.annotation.SaCheckPermission;
import cn.dev33.satoken.annotation.SaCheckSafe;
import cn.dev33.satoken.stp.StpUtil;
import com.sika.code.infrastructure.common.result.Result;
import com.sika.code.test.server.business.auth.constant.AuthUserTypeEnum;
import com.sika.code.test.server.business.auth.pojo.dto.LoginUserDTO;
import com.sika.code.test.server.business.auth.pojo.request.LoginRequest;
import com.sika.code.test.server.business.auth.pojo.request.RegisterRequest;
import com.sika.code.test.server.business.auth.service.AuthService;
import com.sika.code.test.server.common.controller.BaseSikaCodeTestServerController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 认证控制器
 *
 * @author daiqi
 * @create 2021-11-19 1:04
 */
@RestController
@RequestMapping("auth")
public class AuthController extends BaseSikaCodeTestServerController {
    @Autowired
    private AuthService authService;

    @RequestMapping(value = "login")
    public Result login(@RequestBody LoginRequest request) {
        request.setAuthUserType(AuthUserTypeEnum.USER.getType());
        LoginUserDTO loginUserDTO = authService.login(request);
        StpUtil.openSafe(120);
        return success(loginUserDTO);
    }

    @RequestMapping(value = "logout")
    public Result logout() {
        StpUtil.logout();
        return success(true);
    }

    @RequestMapping(value = "isLogin")
    @SaCheckSafe
    public Result isLogin() {
        return success(StpUtil.isLogin());
    }

    @RequestMapping(value = "tokenInfo")
    @SaCheckPermission("us1er:add11")
    public Result tokenInfo() {
        return success(StpUtil.getTokenInfo());
    }

    @RequestMapping(value = "register")
    public Result register(@RequestBody RegisterRequest request) {
        request.setAuthUserType(AuthUserTypeEnum.USER.getType());
        return success(authService.register(request));
    }
}
