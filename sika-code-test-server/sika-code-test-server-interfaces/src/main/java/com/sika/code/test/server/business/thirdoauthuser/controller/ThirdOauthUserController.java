package com.sika.code.test.server.business.thirdoauthuser.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import com.sika.code.infrastructure.common.result.Result;
import com.sika.code.test.server.business.thirdoauthuser.pojo.query.ThirdOauthUserQuery;
import com.sika.code.test.server.business.thirdoauthuser.pojo.dto.ThirdOauthUserDTO;
import com.sika.code.test.server.business.thirdoauthuser.service.ThirdOauthUserService;

import com.sika.code.test.server.common.controller.BaseSikaCodeTestServerController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 第三方授权用户表 前端控制器
 * </p>
 *
 * @author sikadai
 * @since 2021-11-11 00:29:51
 */
@RestController
@RequestMapping("thirdOauthUser")
public class ThirdOauthUserController extends BaseSikaCodeTestServerController {
    @Autowired
    private ThirdOauthUserService thirdOauthUserService;

    @RequestMapping(value = "save")
    public Result save(@RequestBody ThirdOauthUserDTO dto) {
        return success(thirdOauthUserService.save(dto));
    }


    @RequestMapping(value = "save_batch")
    public Result saveBatch(@RequestBody List<ThirdOauthUserDTO> dtos) {
         return success(thirdOauthUserService.saveBatch(dtos));
    }

    @RequestMapping(value = "page")
    public Result page(@RequestBody ThirdOauthUserQuery query) {
        return success(thirdOauthUserService.page(query));
    }

    @RequestMapping(value = "find")
    public Result find(@RequestBody ThirdOauthUserQuery query) {
        return success(thirdOauthUserService.find(query));
    }

    @RequestMapping(value = "list")
    public Result list(@RequestBody ThirdOauthUserQuery query) {
        return success(thirdOauthUserService.list(query));
    }
}
