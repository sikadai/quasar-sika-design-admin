package com.sika.code.test.server.business.testuser.controller;


import com.sika.code.infrastructure.common.result.Result;
import com.sika.code.test.server.business.testuser.pojo.dto.TestUserDTO;
import com.sika.code.test.server.business.testuser.pojo.query.TestUserQuery;
import com.sika.code.test.server.business.testuser.service.TestUserService;
import com.sika.code.test.server.common.controller.BaseSikaCodeTestServerController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author sikadai
 * @since 2021-10-31 13:36:19
 */
@RestController
@RequestMapping("testuser")
public class TestUserController extends BaseSikaCodeTestServerController {
    @Autowired
    private TestUserService testUserService;

    @RequestMapping(value = "save")
    public Result save(@RequestBody TestUserDTO dto) {
        return success(testUserService.save(dto));
    }


    @RequestMapping(value = "save_batch")
    public Result saveBatch(@RequestBody List<TestUserDTO> dtos) {
         return success(testUserService.saveBatch(dtos));
    }

    @RequestMapping(value = "page")
    public Result page(@RequestBody TestUserQuery query) {
        return success(testUserService.page(query));
    }

    @RequestMapping(value = "find")
    public Result find(@RequestBody TestUserQuery query) {
        return success(testUserService.find(query));
    }

    @RequestMapping(value = "list")
    public Result list(@RequestBody TestUserQuery query) {
        return success(testUserService.list(query));
    }
}
