package com.sika.code.test.server.business.user.controller;


import com.sika.code.infrastructure.common.result.Result;
import com.sika.code.test.server.business.user.converter.UserConverter;
import com.sika.code.test.server.business.user.pojo.dto.UserDTO;
import com.sika.code.test.server.business.user.pojo.query.UserQuery;
import com.sika.code.test.server.business.user.service.UserService;
import com.sika.code.test.server.common.controller.BaseSikaCodeTestServerController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * <p>
 * 用户表 前端控制器
 * </p>
 *
 * @author sikadai
 * @since 2021-10-31 17:45:09
 */
@RestController
@RequestMapping("user")
public class UserController extends BaseSikaCodeTestServerController {
    @Autowired
    private UserService userService;

    @RequestMapping(value = "save")
    public Result save(@RequestBody UserDTO dto) {
        return success(userService.save(dto));
    }


    @RequestMapping(value = "save_batch")
    public Result saveBatch(@RequestBody List<UserDTO> dtos) {
        return success(userService.saveBatch(dtos));
    }

    @RequestMapping(value = "page")
    public Result page(@RequestBody UserQuery query) {
        return success(userService.page(query));
    }

    @RequestMapping(value = "find")
    public Result find(@RequestBody UserQuery query) {
        return success(userService.find(query));
    }

    @RequestMapping(value = "list")
    public Result list(@RequestBody UserQuery query) {
        return success(userService.list(query));
    }

    @RequestMapping(value = "convert")
    public Result convert(@RequestBody UserQuery query) {
        if (query.getId() == null) {
            query.setId(1000000L);
        }
//        UserConverter.convertToDtosUseHutool(query);
//        UserConverter.convertToDtosUseSpring(query);
//        UserConverter.convertToDtosUseJon(query);
        UserConverter.convertToDtosUseCglibUtil(query);
        UserConverter.convertToDtosUseOrginar(query);
        UserConverter.convertToDtosUseMapStruct(query);
        return success(1);
    }

}
