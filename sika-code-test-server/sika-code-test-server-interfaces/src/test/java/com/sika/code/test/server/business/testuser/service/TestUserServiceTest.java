package com.sika.code.test.server.business.testuser.service;

import com.sika.code.infractructure.db.util.HintManagerHandler;
import com.sika.code.infrastructure.common.base.constant.BaseConstant;
import com.sika.code.infrastructure.common.base.test.BaseTestService;
import com.sika.code.infrastructure.common.base.util.JSONUtil;
import com.sika.code.test.server.business.testuser.pojo.dto.TestUserDTO;
import com.sika.code.test.server.business.testuser.pojo.query.TestUserQuery;
import com.sika.code.test.server.business.testuser.pojo.valueobject.UpdateUserNameValueObject;
import org.apache.shardingsphere.api.hint.HintManager;
import org.assertj.core.util.Lists;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Date;
import java.util.List;
import java.util.UUID;

/**
 * @author daiqi
 * @create 2021-10-20 0:26
 */
public class TestUserServiceTest extends BaseTestService {
    @Autowired
    private TestUserService testUserService;


    @Test
    public void testFind() {
        TestUserQuery query = new TestUserQuery();
        query.setId(21104L);
        TestUserDTO testUserDTO = testUserService.find(query);
        log.info("testUserDTO:{}", JSONUtil.toJSONString(testUserDTO));

    }

    @Test
    public void testSaveBatch() {
        List<TestUserDTO> testUserDtos = Lists.newArrayList();
        for (int i = 0; i < 1000; ++i) {
            TestUserDTO testUserDTO = new TestUserDTO();
            String value = (10000 + i) + "012";
            testUserDTO.setUserId(Long.valueOf(value));
            testUserDTO.setUserName(UUID.randomUUID().toString());
            testUserDTO.setIsDeleted(BaseConstant.IsDeletedEnum.NO.getType());
            testUserDTO.setVersion(0);
            testUserDTO.setUpdateDate(new Date());
            testUserDTO.setCreateDate(new Date());
            testUserDTO.setAvailable(0);
            testUserDTO.setRemark("");
            testUserDtos.add(testUserDTO);
        }
        try (HintManager hintManager = HintManagerHandler.getInstanceBeforeClear()) {
            HintManagerHandler.addDatabaseShardingValue(hintManager, "01");
            HintManagerHandler.addTableShardingValue(hintManager, "2");
            testUserService.saveBatch(testUserDtos);
        }

    }


    @Test
    public void testUpdateUserName() {
        UpdateUserNameValueObject valueObject = new UpdateUserNameValueObject();
        valueObject.setId(21104L);
        valueObject.setUserName("这是个测2哦哦哦222222222哦");
        TestUserDTO testUserDTO = testUserService.updateUserName(valueObject);
        log.info("testUserDTO:{}", JSONUtil.toJSONString(testUserDTO));

    }
}
