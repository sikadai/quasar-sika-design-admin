package com.sika.code.test.server.business.user.service;


import com.google.common.collect.Lists;
import com.sika.code.infrastructure.common.base.pojo.query.Page;
import com.sika.code.infrastructure.common.base.test.BaseTestService;
import com.sika.code.test.server.business.auth.pojo.dto.LoginUserDTO;
import com.sika.code.test.server.business.auth.service.AuthService;
import com.sika.code.test.server.business.user.pojo.dto.UserDTO;
import com.sika.code.test.server.business.user.pojo.query.UserQuery;
import com.sika.code.test.server.business.auth.pojo.request.LoginRequest;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

/**
 * <p>
 * 用户表 服务测试类
 * </p>
 *
 * @author sikadai
 * @since 2021-10-31 17:45:11
 */
public class TestUserService extends BaseTestService {
    @Autowired
    private UserService userService;
    @Autowired
    private AuthService authService;

    @Test
    public void testLogin() {
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setLoginCredentials("meetrice@qq.com");
//        loginRequest.setLoginCredentials("15312083732");
//        loginRequest.setLoginCredentials("sika");
        loginRequest.setLoginKey("123456");
        LoginUserDTO userDTO = authService.login(loginRequest);
        Assert.assertTrue(userDTO != null);
    }

    @Test
    public void testFindByPrimaryKey() {
        Long key = 1L;
        UserDTO userDTO = userService.findByPrimaryKey(key);
        Assert.assertNotNull(userDTO);
    }

    @Test
    public void testSaveAndRet() {
        UserDTO userDTO = buildUserDTO();
        UserDTO userDTORet = userService.saveAndRet(userDTO);
        Assert.assertNotNull(userDTORet);
    }

    @Test
    public void testSave() {
        UserDTO userDTO = buildUserDTO();
        boolean result = userService.save(userDTO);
        Assert.assertTrue(result);
    }

    @Test
    public void testSaveBatchAndRet() {
        List<UserDTO> pos = Lists.newArrayList();
        for (int i = 0; i < 10; ++i) {
            UserDTO userDTO = buildUserDTO();
            pos.add(userDTO);
        }
        List<UserDTO> posRet = userService.saveBatchAndRet(pos);
        Assert.assertTrue(posRet.size() > 0);
    }

    @Test
    public void testSaveBatch() {
        List<UserDTO> pos = Lists.newArrayList();
        for (int i = 0; i < 10; ++i) {
            UserDTO userDTO = buildUserDTO();
            pos.add(userDTO);
        }
        boolean result = userService.saveBatch(pos);
        Assert.assertTrue(result);
    }

    @Test
    public void testUpdateAndRet() {
        UserDTO userDTO = buildUserDTO();
        userDTO.setId(null);
        UserDTO userDTORet = userService.saveAndRet(userDTO);
        Assert.assertNotNull(userDTORet);
    }

    @Test
    public void testUpdateByPrimaryKey() {
        UserDTO userDTO = buildUserDTO();
        userDTO.setId(null);
        boolean result = userService.save(userDTO);
        Assert.assertTrue(result);
    }

    @Test
    public void testUpdateBatchAndRet() {
        List<UserDTO> pos = Lists.newArrayList();
        for (int i = 0; i < 10; ++i) {
            UserDTO userDTO = buildUserDTO();
            pos.add(userDTO);
        }
        List<UserDTO> posRet = userService.saveBatchAndRet(pos);
        Assert.assertTrue(posRet.size() > 0);
    }

    @Test
    public void testUpdateBatch() {
        List<UserDTO> pos = Lists.newArrayList();
        for (int i = 0; i < 10; ++i) {
            UserDTO userDTO = buildUserDTO();
            pos.add(userDTO);
        }
        boolean result = userService.saveBatch(pos);
        Assert.assertTrue(result);
    }

    @Test
    public void testSaveOrUpdateAndRet() {
        UserDTO userDTO = buildUserDTO();
        UserDTO userDTORet = userService.saveAndRet(userDTO);
        Assert.assertNotNull(userDTORet);
    }

    @Test
    public void testSaveOrUpdate() {
        UserDTO userDTO = buildUserDTO();
        boolean result = userService.save(userDTO);
        Assert.assertTrue(result);
    }

    @Test
    public void testSaveOrUpdateBatchAndRet() {
        List<UserDTO> pos = Lists.newArrayList();
        for (int i = 0; i < 10; ++i) {
            UserDTO userDTO = buildUserDTO();
            pos.add(userDTO);
        }
        List<UserDTO> posRet = userService.saveBatchAndRet(pos);
        Assert.assertTrue(posRet.size() > 0);
    }

    @Test
    public void testSaveOrUpdateBatch() {
        List<UserDTO> pos = Lists.newArrayList();
        for (int i = 0; i < 10; ++i) {
            UserDTO userDTO = buildUserDTO();
            pos.add(userDTO);
        }
        boolean result = userService.saveBatch(pos);
        Assert.assertTrue(result);
    }

    @Test
    public void testFind() {
        UserQuery userQuery = buildUserQuery();
        UserDTO userDTORet = userService.find(userQuery);
        Assert.assertNotNull(userDTORet);
    }

    @Test
    public void testFindId() {
        UserQuery userQuery = buildUserQuery();
        Long key = userService.findId(userQuery);
        Assert.assertNotNull(key);
    }

    @Test
    public void testList() {
        UserQuery userQuery = buildUserQuery();
        List<UserDTO> userDTOS = userService.list(userQuery);
        Assert.assertTrue(userDTOS.size() > 0);
    }

    @Test
    public void testListId() {
        UserQuery userQuery = buildUserQuery();
        List<Long> keys = userService.listId(userQuery);
        Assert.assertTrue(keys.size() > 0);
    }

    @Test
    public void testPage() {
        UserQuery userQuery = buildUserQuery();
        Page<UserDTO> page = userService.page(userQuery);
        Assert.assertTrue(page.getTotal() > 0);
    }


    private UserDTO buildUserDTO() {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(null);
        userDTO.setCreateBy(null);
        userDTO.setUpdateBy(null);
        userDTO.setUsername(null);
        userDTO.setPassword(null);
        userDTO.setOauthPwd(null);
        userDTO.setNickname(null);
        userDTO.setSex(null);
        userDTO.setPhone(null);
        userDTO.setEmail(null);
        userDTO.setAvatar(null);
        userDTO.setProvinceCode(null);
        userDTO.setToken(null);
        userDTO.setType(null);
        userDTO.setCityCode(null);
        userDTO.setCountyCode(null);
        userDTO.setAddress(null);
        return userDTO;
    }

    private UserQuery buildUserQuery() {
        UserQuery userQuery = new UserQuery();
        userQuery.setId(null);
        userQuery.setCreateBy(null);
        userQuery.setUpdateBy(null);
        userQuery.setUsername(null);
        userQuery.setPassword(null);
        userQuery.setOauthPwd(null);
        userQuery.setNickname(null);
        userQuery.setSex(null);
        userQuery.setPhone(null);
        userQuery.setEmail(null);
        userQuery.setAvatar(null);
        userQuery.setProvinceCode(null);
        userQuery.setToken(null);
        userQuery.setType(null);
        userQuery.setCityCode(null);
        userQuery.setCountyCode(null);
        userQuery.setAddress(null);
        return userQuery;
    }
}