package com.sika.code.test.server.business.testuser.controller;

import com.alibaba.fastjson.JSONObject;
import com.sika.code.infrastructure.common.base.test.BaseTestController;
import com.sika.code.infrastructure.common.result.Result;
import com.sika.code.test.server.business.testuser.pojo.query.TestUserQuery;
import org.junit.Test;

/**
 * @author daiqi
 * @create 2021-10-20 0:30
 */
public class TestUserControllerTest extends BaseTestController {

    @Test
    public void find() throws Exception {
        //创建新用户
        TestUserQuery userParam = new TestUserQuery();
        userParam.setUserId(42012L);
        Result result = postJson("/testuser/find", userParam);
        log.info("result:{}", JSONObject.toJSONString(result));
    }
}
