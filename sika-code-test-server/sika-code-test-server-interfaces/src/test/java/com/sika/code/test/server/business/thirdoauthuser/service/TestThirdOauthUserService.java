package com.sika.code.test.server.business.thirdoauthuser.service;


import com.sika.code.infrastructure.common.base.test.BaseTestService;
import com.sika.code.infrastructure.common.base.pojo.query.Page;
import com.google.common.collect.Lists;
import com.sika.code.test.server.business.thirdoauthuser.service.ThirdOauthUserService;
import com.sika.code.test.server.business.thirdoauthuser.pojo.dto.ThirdOauthUserDTO;
import com.sika.code.test.server.business.thirdoauthuser.pojo.query.ThirdOauthUserQuery;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;

/**
 * <p>
 * 第三方授权用户表 服务测试类
 * </p>
 *
 * @author sikadai
 * @since 2021-11-11 00:22:38
 */
public class TestThirdOauthUserService extends BaseTestService {
    @Autowired
    private ThirdOauthUserService thirdOauthUserService;

    @Test
    public void testFindByPrimaryKey() {
        Long key = 1L;
        ThirdOauthUserDTO thirdOauthUserDTO = thirdOauthUserService.findByPrimaryKey(key);
        Assert.assertNotNull(thirdOauthUserDTO);
    }

    @Test
    public void testSaveAndRet() {
        ThirdOauthUserDTO thirdOauthUserDTO = buildThirdOauthUserDTO();
        ThirdOauthUserDTO thirdOauthUserDTORet = thirdOauthUserService.saveAndRet(thirdOauthUserDTO);
        Assert.assertNotNull(thirdOauthUserDTORet);
    }

    @Test
    public void testSave() {
        ThirdOauthUserDTO thirdOauthUserDTO = buildThirdOauthUserDTO();
        boolean result = thirdOauthUserService.save(thirdOauthUserDTO);
        Assert.assertTrue(result);
    }

    @Test
    public void testSaveBatchAndRet() {
        List<ThirdOauthUserDTO> pos = Lists.newArrayList();
        for (int i = 0; i < 10; ++i) {
            ThirdOauthUserDTO thirdOauthUserDTO = buildThirdOauthUserDTO();
            pos.add(thirdOauthUserDTO);
        }
        List<ThirdOauthUserDTO> posRet = thirdOauthUserService.saveBatchAndRet(pos);
        Assert.assertTrue(posRet.size() > 0);
    }

    @Test
    public void testSaveBatch() {
        List<ThirdOauthUserDTO> pos = Lists.newArrayList();
        for (int i = 0; i < 10; ++i) {
            ThirdOauthUserDTO thirdOauthUserDTO = buildThirdOauthUserDTO();
            pos.add(thirdOauthUserDTO);
        }
        boolean result = thirdOauthUserService.saveBatch(pos);
        Assert.assertTrue(result);
    }

    @Test
    public void testUpdateAndRet() {
        ThirdOauthUserDTO thirdOauthUserDTO = buildThirdOauthUserDTO();
        thirdOauthUserDTO.setId(null);
        ThirdOauthUserDTO thirdOauthUserDTORet = thirdOauthUserService.saveAndRet(thirdOauthUserDTO);
        Assert.assertNotNull(thirdOauthUserDTORet);
    }

    @Test
    public void testUpdateByPrimaryKey() {
        ThirdOauthUserDTO thirdOauthUserDTO = buildThirdOauthUserDTO();
        thirdOauthUserDTO.setId(null);
        boolean result = thirdOauthUserService.save(thirdOauthUserDTO);
        Assert.assertTrue(result);
    }

    @Test
    public void testUpdateBatchAndRet() {
        List<ThirdOauthUserDTO> pos = Lists.newArrayList();
        for (int i = 0; i < 10; ++i) {
            ThirdOauthUserDTO thirdOauthUserDTO = buildThirdOauthUserDTO();
            pos.add(thirdOauthUserDTO);
        }
        List<ThirdOauthUserDTO> posRet = thirdOauthUserService.saveBatchAndRet(pos);
        Assert.assertTrue(posRet.size() > 0);
    }

    @Test
    public void testUpdateBatch() {
        List<ThirdOauthUserDTO> pos = Lists.newArrayList();
        for (int i = 0; i < 10; ++i) {
            ThirdOauthUserDTO thirdOauthUserDTO = buildThirdOauthUserDTO();
            pos.add(thirdOauthUserDTO);
        }
        boolean result = thirdOauthUserService.saveBatch(pos);
        Assert.assertTrue(result);
    }

    @Test
    public void testSaveOrUpdateAndRet() {
        ThirdOauthUserDTO thirdOauthUserDTO = buildThirdOauthUserDTO();
        ThirdOauthUserDTO thirdOauthUserDTORet = thirdOauthUserService.saveAndRet(thirdOauthUserDTO);
        Assert.assertNotNull(thirdOauthUserDTORet);
    }

    @Test
    public void testSaveOrUpdate() {
        ThirdOauthUserDTO thirdOauthUserDTO = buildThirdOauthUserDTO();
        boolean result = thirdOauthUserService.save(thirdOauthUserDTO);
        Assert.assertTrue(result);
    }

    @Test
    public void testSaveOrUpdateBatchAndRet() {
        List<ThirdOauthUserDTO> pos = Lists.newArrayList();
        for (int i = 0; i < 10; ++i) {
            ThirdOauthUserDTO thirdOauthUserDTO = buildThirdOauthUserDTO();
            pos.add(thirdOauthUserDTO);
        }
        List<ThirdOauthUserDTO> posRet = thirdOauthUserService.saveBatchAndRet(pos);
        Assert.assertTrue(posRet.size() > 0);
    }

    @Test
    public void testSaveOrUpdateBatch() {
        List<ThirdOauthUserDTO> pos = Lists.newArrayList();
        for (int i = 0; i < 10; ++i) {
            ThirdOauthUserDTO thirdOauthUserDTO = buildThirdOauthUserDTO();
            pos.add(thirdOauthUserDTO);
        }
        boolean result = thirdOauthUserService.saveBatch(pos);
        Assert.assertTrue(result);
    }

    @Test
    public void testFind() {
        ThirdOauthUserQuery thirdOauthUserQuery = buildThirdOauthUserQuery();
        ThirdOauthUserDTO thirdOauthUserDTORet = thirdOauthUserService.find(thirdOauthUserQuery);
        Assert.assertNotNull(thirdOauthUserDTORet);
    }

    @Test
    public void testFindId() {
        ThirdOauthUserQuery thirdOauthUserQuery = buildThirdOauthUserQuery();
        Long key = thirdOauthUserService.findId(thirdOauthUserQuery);
        Assert.assertNotNull(key);
    }

    @Test
    public void testList() {
        ThirdOauthUserQuery thirdOauthUserQuery = buildThirdOauthUserQuery();
        List<ThirdOauthUserDTO> thirdOauthUserDTOS = thirdOauthUserService.list(thirdOauthUserQuery);
        Assert.assertTrue(thirdOauthUserDTOS.size() > 0);
    }

    @Test
    public void testListId() {
        ThirdOauthUserQuery thirdOauthUserQuery = buildThirdOauthUserQuery();
        List<Long> keys = thirdOauthUserService.listId(thirdOauthUserQuery);
        Assert.assertTrue(keys.size() > 0);
    }

    @Test
    public void testPage() {
        ThirdOauthUserQuery thirdOauthUserQuery = buildThirdOauthUserQuery();
        Page<ThirdOauthUserDTO> page = thirdOauthUserService.page(thirdOauthUserQuery);
        Assert.assertTrue(page.getTotal() > 0);
    }


    private ThirdOauthUserDTO buildThirdOauthUserDTO() {
        ThirdOauthUserDTO thirdOauthUserDTO = new ThirdOauthUserDTO();
        thirdOauthUserDTO.setId(null);
        thirdOauthUserDTO.setCreateBy(null);
        thirdOauthUserDTO.setUpdateBy(null);
        thirdOauthUserDTO.setUuid(null);
        thirdOauthUserDTO.setUsername(null);
        thirdOauthUserDTO.setNickname(null);
        thirdOauthUserDTO.setAvatar(null);
        thirdOauthUserDTO.setBlog(null);
        thirdOauthUserDTO.setCompany(null);
        thirdOauthUserDTO.setLocation(null);
        thirdOauthUserDTO.setEmail(null);
        thirdOauthUserDTO.setGender(null);
        thirdOauthUserDTO.setSource(null);
        thirdOauthUserDTO.setToken(null);
        thirdOauthUserDTO.setRawUserInfo(null);
        thirdOauthUserDTO.setUserId(null);
        thirdOauthUserDTO.setState(null);
        return thirdOauthUserDTO;
    }

    private ThirdOauthUserQuery buildThirdOauthUserQuery() {
        ThirdOauthUserQuery thirdOauthUserQuery = new ThirdOauthUserQuery();
        thirdOauthUserQuery.setId(null);
        thirdOauthUserQuery.setCreateBy(null);
        thirdOauthUserQuery.setUpdateBy(null);
        thirdOauthUserQuery.setUuid(null);
        thirdOauthUserQuery.setUsername(null);
        thirdOauthUserQuery.setNickname(null);
        thirdOauthUserQuery.setAvatar(null);
        thirdOauthUserQuery.setBlog(null);
        thirdOauthUserQuery.setCompany(null);
        thirdOauthUserQuery.setLocation(null);
        thirdOauthUserQuery.setEmail(null);
        thirdOauthUserQuery.setGender(null);
        thirdOauthUserQuery.setSource(null);
        thirdOauthUserQuery.setToken(null);
        thirdOauthUserQuery.setRawUserInfo(null);
        thirdOauthUserQuery.setUserId(null);
        thirdOauthUserQuery.setState(null);
        return thirdOauthUserQuery;
    }
}