package com.sika.code.test.server.business.testuser.repository;

import com.sika.code.infrastructure.common.base.test.BaseTestRepository;
import com.sika.code.infrastructure.common.base.test.BaseTestService;
import com.sika.code.infrastructure.common.base.util.JSONUtil;
import com.sika.code.test.server.business.testuser.db.repository.TestUserRepository;
import com.sika.code.test.server.business.testuser.pojo.dto.TestUserDTO;
import com.sika.code.test.server.business.testuser.pojo.po.TestUserPO;
import com.sika.code.test.server.business.testuser.pojo.query.TestUserQuery;
import com.sika.code.test.server.business.testuser.service.TestUserService;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

/**
 * @author daiqi
 * @create 2021-10-20 0:26
 */
public class TestUserRepositoryTest extends BaseTestRepository {
    @Autowired
    private TestUserRepository testUserRepository;

    @Test
    public void testFind() {
        TestUserQuery query = new TestUserQuery();
        query.setUserId(10000012L);
        TestUserPO testUserPO = testUserRepository.find(query);
        log.info("testUserPO:{}", JSONUtil.toJSONString(testUserPO));

    }
}
