package com.sika.code.test.server.business.user.controller;


import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.sika.code.infrastructure.common.base.test.BaseTestController;
import com.sika.code.infrastructure.common.result.Result;
import com.sika.code.test.server.business.user.pojo.dto.UserDTO;
import com.sika.code.test.server.business.user.pojo.query.UserQuery;
import com.sika.code.test.server.business.auth.pojo.request.LoginRequest;
import org.junit.Test;

import java.util.List;

/**
 * <p>
 * 用户表控制器测试类
 * </p>
 *
 * @author sikadai
 * @since 2021-10-31 17:45:12
 */
public class TestUserController extends BaseTestController {

    @Test
    public void testLoginByPhoneCode() {
        LoginRequest request = new LoginRequest();
        request.setLoginCredentials("15312083732");
        request.setLoginKey("123456");
        Result result = postJson("/user/login", request);
        log.info("result:{}", JSONObject.toJSONString(result));
    }

    @Test
    public void testLoginByMailCode() {
        LoginRequest request = new LoginRequest();
        request.setLoginCredentials("meetrice@qq.com");
        request.setLoginKey("123456");
        Result result = postJson("/user/login", request);
        log.info("result:{}", JSONObject.toJSONString(result));
    }

    @Test
    public void testLoginByPwd() {
        LoginRequest request = new LoginRequest();
        request.setLoginCredentials("si1ka");
        request.setLoginKey("123456");
        Result result = postJson("/user/login", request);
        log.info("result:{}", JSONObject.toJSONString(result));
    }

    @Test
    public void testSave() {
        UserDTO userDTO = buildUserDTO();
        Result result = postJson("/user/save", userDTO);
        log.info("result:{}", JSONObject.toJSONString(result));
    }

    @Test
    public void testSaveBatch() {
        List<UserDTO> DTOs = Lists.newArrayList();
        for (int i = 0; i < 10; ++i) {
            UserDTO userDTO = buildUserDTO();
            DTOs.add(userDTO);
        }
        Result result = postJson("/user/save_batch", DTOs);
        log.info("result:{}", JSONObject.toJSONString(result));
    }

    @Test
    public void testUpdateById() {
        UserDTO userDTO = buildUserDTO();
        Result result = postJson("/user/save", userDTO);
        log.info("result:{}", JSONObject.toJSONString(result));
    }

    @Test
    public void testPage() {
        UserQuery query = buildUserQuery();
        Result result = postJson("/user/page", query);
        log.info("result:{}", JSONObject.toJSONString(result));
    }

    @Test
    public void testFind() {
        UserQuery query = buildUserQuery();
        Result result = postJson("/user/find", query);
        log.info("result:{}", JSONObject.toJSONString(result));
    }

    @Test
    public void testList() {
        UserQuery query = buildUserQuery();
        Result result = postJson("/user/list", query);
        log.info("result:{}", JSONObject.toJSONString(result));
    }

    @Test
    public void convertTest() {
        UserQuery query = buildUserQuery();
        query.setId(1000000L);
        query.setAddress("eeeeeeeeeeeeeeeeeeeeeeeeeeeee");
        query.setPhone("1893484878");
        query.setEmail("1893484878");
        query.setUsername("zhangsan");
        Result result = postJson("/user/convert", query);
        log.info("result:{}", JSONObject.toJSONString(result));
    }

    private UserDTO buildUserDTO() {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(null);
        userDTO.setCreateBy(null);
        userDTO.setUpdateBy(null);
        userDTO.setUsername(null);
        userDTO.setPassword(null);
        userDTO.setOauthPwd(null);
        userDTO.setNickname(null);
        userDTO.setSex(null);
        userDTO.setPhone(null);
        userDTO.setEmail(null);
        userDTO.setAvatar(null);
        userDTO.setProvinceCode(null);
        userDTO.setToken(null);
        userDTO.setType(null);
        userDTO.setCityCode(null);
        userDTO.setCountyCode(null);
        userDTO.setAddress(null);
        return userDTO;
    }


    private UserQuery buildUserQuery() {
        UserQuery userQuery = new UserQuery();
        userQuery.setId(null);
        userQuery.setCreateBy(null);
        userQuery.setUpdateBy(null);
        userQuery.setUsername(null);
        userQuery.setPassword(null);
        userQuery.setOauthPwd(null);
        userQuery.setNickname(null);
        userQuery.setSex(null);
        userQuery.setPhone(null);
        userQuery.setEmail(null);
        userQuery.setAvatar(null);
        userQuery.setProvinceCode(null);
        userQuery.setToken(null);
        userQuery.setType(null);
        userQuery.setCityCode(null);
        userQuery.setCountyCode(null);
        userQuery.setAddress(null);
        return userQuery;
    }

}