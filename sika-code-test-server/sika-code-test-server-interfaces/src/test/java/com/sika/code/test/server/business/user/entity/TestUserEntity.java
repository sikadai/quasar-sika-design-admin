package com.sika.code.test.server.business.user.entity;


import com.sika.code.test.server.business.user.pojo.dto.UserDTO;
import com.sika.code.infrastructure.common.base.test.BaseTestEntity;

/**
 * <p>
 * 用户表领域测试类
 * </p>
 *
 * @author sikadai
 * @since 2021-10-31 17:45:11
 */
public class TestUserEntity extends BaseTestEntity {

}