package com.sika.code.test.server.business.testuser.entity;

import com.sika.code.infrastructure.common.base.test.BaseTestEntity;
import com.sika.code.infrastructure.common.base.util.JSONUtil;
import com.sika.code.test.server.business.testuser.domain.entity.UpdateUserNameEntity;
import com.sika.code.test.server.business.testuser.domain.factory.TestUserFactory;
import com.sika.code.test.server.business.testuser.pojo.dto.TestUserDTO;
import com.sika.code.test.server.business.testuser.pojo.valueobject.UpdateUserNameValueObject;
import org.junit.Test;

/**
 * @author daiqi
 * @create 2021-10-20 0:26
 */
public class TestUserEntityTest extends BaseTestEntity {

    @Test
    public void testUpdateUserName() {
        UpdateUserNameValueObject valueObject = new UpdateUserNameValueObject();
        valueObject.setUserId(10000012L);
        valueObject.setUserName("这是个2测试2的哦1哦哦哦");
        UpdateUserNameEntity entity = TestUserFactory.getUpdateUserNameEntity(valueObject);
        TestUserDTO testUserDTO = entity.execute();
        log.info("testUserDTO:{}", JSONUtil.toJSONString(testUserDTO));

    }
}
