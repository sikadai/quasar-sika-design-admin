package com.sika.code.test.server.business.thirdoauthuser.controller;


import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.sika.code.infrastructure.common.base.test.BaseTestController;
import com.sika.code.infrastructure.common.result.Result;
import com.sika.code.test.server.business.thirdoauthuser.pojo.dto.ThirdOauthUserDTO;
import com.sika.code.test.server.business.thirdoauthuser.pojo.query.ThirdOauthUserQuery;
import org.junit.Test;

import java.util.List;

/**
 * <p>
 * 第三方授权用户表控制器测试类
 * </p>
 *
 * @author sikadai
 * @since 2021-11-11 00:22:39
 */
public class TestThirdOauthUserController extends BaseTestController {

    @Test
    public void testSave() {
        ThirdOauthUserDTO thirdOauthUserDTO = buildThirdOauthUserDTO();
        Result result = postJson("/thirdOauthUser/save", thirdOauthUserDTO);
        log.info("result:{}", JSONObject.toJSONString(result));
    }

    @Test
    public void testSaveBatch() {
    List<ThirdOauthUserDTO> DTOs = Lists.newArrayList();
        for (int i = 0; i < 10; ++i) {
            ThirdOauthUserDTO thirdOauthUserDTO = buildThirdOauthUserDTO();
            DTOs.add(thirdOauthUserDTO);
        }
        Result result = postJson("/thirdOauthUser/save_batch", DTOs);
        log.info("result:{}", JSONObject.toJSONString(result));
    }

    @Test
    public void testUpdateById() {
        ThirdOauthUserDTO thirdOauthUserDTO = buildThirdOauthUserDTO();
        Result result = postJson("/thirdOauthUser/save", thirdOauthUserDTO);
        log.info("result:{}", JSONObject.toJSONString(result));
    }

    @Test
    public void testPage() {
        ThirdOauthUserQuery query = buildThirdOauthUserQuery();
        Result result = postJson("/thirdOauthUser/page", query);
        log.info("result:{}", JSONObject.toJSONString(result));
    }

    @Test
    public void testFind() {
        ThirdOauthUserQuery query = buildThirdOauthUserQuery();
        Result result = postJson("/thirdOauthUser/find", query);
        log.info("result:{}", JSONObject.toJSONString(result));
    }

    @Test
    public void testList() {
        ThirdOauthUserQuery query = buildThirdOauthUserQuery();
        Result result = postJson("/thirdOauthUser/list", query);
        log.info("result:{}", JSONObject.toJSONString(result));
    }

    private ThirdOauthUserDTO buildThirdOauthUserDTO() {
        ThirdOauthUserDTO thirdOauthUserDTO = new ThirdOauthUserDTO();
        thirdOauthUserDTO.setId(null);
        thirdOauthUserDTO.setCreateBy(null);
        thirdOauthUserDTO.setUpdateBy(null);
        thirdOauthUserDTO.setUuid(null);
        thirdOauthUserDTO.setUsername(null);
        thirdOauthUserDTO.setNickname(null);
        thirdOauthUserDTO.setAvatar(null);
        thirdOauthUserDTO.setBlog(null);
        thirdOauthUserDTO.setCompany(null);
        thirdOauthUserDTO.setLocation(null);
        thirdOauthUserDTO.setEmail(null);
        thirdOauthUserDTO.setGender(null);
        thirdOauthUserDTO.setSource(null);
        thirdOauthUserDTO.setToken(null);
        thirdOauthUserDTO.setRawUserInfo(null);
        thirdOauthUserDTO.setUserId(null);
        thirdOauthUserDTO.setState(null);
        return thirdOauthUserDTO;
    }

    private ThirdOauthUserQuery buildThirdOauthUserQuery() {
        ThirdOauthUserQuery thirdOauthUserQuery = new ThirdOauthUserQuery();
        thirdOauthUserQuery.setId(null);
        thirdOauthUserQuery.setCreateBy(null);
        thirdOauthUserQuery.setUpdateBy(null);
        thirdOauthUserQuery.setUuid(null);
        thirdOauthUserQuery.setUsername(null);
        thirdOauthUserQuery.setNickname(null);
        thirdOauthUserQuery.setAvatar(null);
        thirdOauthUserQuery.setBlog(null);
        thirdOauthUserQuery.setCompany(null);
        thirdOauthUserQuery.setLocation(null);
        thirdOauthUserQuery.setEmail(null);
        thirdOauthUserQuery.setGender(null);
        thirdOauthUserQuery.setSource(null);
        thirdOauthUserQuery.setToken(null);
        thirdOauthUserQuery.setRawUserInfo(null);
        thirdOauthUserQuery.setUserId(null);
        thirdOauthUserQuery.setState(null);
        return thirdOauthUserQuery;
    }
}