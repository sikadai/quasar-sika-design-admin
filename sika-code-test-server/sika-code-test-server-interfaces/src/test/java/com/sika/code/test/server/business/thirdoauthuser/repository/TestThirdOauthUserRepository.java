package com.sika.code.test.server.business.thirdoauthuser.repository;


import com.sika.code.infrastructure.common.base.test.BaseTestRepository;
import com.google.common.collect.Lists;
import com.sika.code.test.server.business.thirdoauthuser.db.repository.ThirdOauthUserRepository;
import com.sika.code.test.server.business.thirdoauthuser.pojo.po.ThirdOauthUserPO;
import com.sika.code.test.server.business.thirdoauthuser.pojo.query.ThirdOauthUserQuery;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import java.util.List;

/**
 * <p>
 * 第三方授权用户表持久化操作测试类
 * </p>
 *
 * @author sikadai
 * @since 2021-11-11 00:40:56
 */
public class TestThirdOauthUserRepository extends BaseTestRepository {
    @Autowired
    private ThirdOauthUserRepository thirdOauthUserRepository;

    @Test
    public void testFindByPrimaryKey() {
        Long primaryKey = 1L;
        ThirdOauthUserPO thirdOauthUserPO = thirdOauthUserRepository.findByPrimaryKey(primaryKey);
        Assert.assertNotNull(thirdOauthUserPO);
    }

    @Test
    public void testUpdateSelectiveByPrimaryKey() {
        ThirdOauthUserPO thirdOauthUserPO = buildThirdOauthUserPO();
        thirdOauthUserPO.setId(null);
        int count = thirdOauthUserRepository.save(thirdOauthUserPO);
        Assert.assertTrue(count > 0);
    }

    @Test
    public void testInsertSelectiveRetPrimaryKey() {
        ThirdOauthUserPO thirdOauthUserPO = buildThirdOauthUserPO();
        Long primaryKey = thirdOauthUserRepository.saveRetId(thirdOauthUserPO);
        Assert.assertTrue(primaryKey > 0);
    }

    @Test
    public void testInsertSelective() {
        ThirdOauthUserPO thirdOauthUserPO = buildThirdOauthUserPO();
        int count = thirdOauthUserRepository.save(thirdOauthUserPO);
        Assert.assertTrue(count > 0);
    }

    @Test
    public void testInsertBatchSelective() {
        List<ThirdOauthUserPO> pos = Lists.newArrayList();
        for (int i = 0; i < 10; ++i) {
            ThirdOauthUserPO thirdOauthUserPO = buildThirdOauthUserPO();
            pos.add(thirdOauthUserPO);
        }
        int count = thirdOauthUserRepository.saveBatch(pos);
        Assert.assertTrue(count > 0);
    }


    @Test
    public void testUpdateBatchSelectiveByPrimaryKey() {
        List<ThirdOauthUserPO> pos = Lists.newArrayList();
        for (int i = 0; i < 10; ++i) {
            ThirdOauthUserPO thirdOauthUserPO = buildThirdOauthUserPO();
            pos.add(thirdOauthUserPO);
        }
        int count = thirdOauthUserRepository.saveBatch(pos);
        Assert.assertTrue(count > 0);
    }

    @Test
    public void testFind() {
        ThirdOauthUserQuery thirdOauthUserQuery = buildThirdOauthUserQuery();
        ThirdOauthUserPO po = thirdOauthUserRepository.find(thirdOauthUserQuery);
        Assert.assertNotNull(po);
    }

    @Test
    public void testFindId() {
        ThirdOauthUserQuery thirdOauthUserQuery = buildThirdOauthUserQuery();
        Long primaryKey = thirdOauthUserRepository.findId(thirdOauthUserQuery);
        Assert.assertTrue(primaryKey > 0);
    }

    @Test
    public void testList() {
        ThirdOauthUserQuery thirdOauthUserQuery = buildThirdOauthUserQuery();
        List<ThirdOauthUserPO> pos = thirdOauthUserRepository.list(thirdOauthUserQuery);
        Assert.assertTrue(pos.size() > 0);
    }

    @Test
    public void testListId() {
        ThirdOauthUserQuery thirdOauthUserQuery = buildThirdOauthUserQuery();
        List<Long> primarys = thirdOauthUserRepository.listId(thirdOauthUserQuery);
        Assert.assertTrue(primarys.size() > 0);
    }

    @Test
    public void testPage() {
        ThirdOauthUserQuery thirdOauthUserQuery = buildThirdOauthUserQuery();
        List<ThirdOauthUserPO> pos = thirdOauthUserRepository.page(thirdOauthUserQuery);
        Assert.assertTrue(pos.size() > 0);
    }

    @Test
    public void testCount() {
        ThirdOauthUserQuery thirdOauthUserQuery = buildThirdOauthUserQuery();
        int count = thirdOauthUserRepository.count(thirdOauthUserQuery);
        Assert.assertTrue(count > 0);
    }


    private ThirdOauthUserPO buildThirdOauthUserPO() {
        ThirdOauthUserPO thirdOauthUserPO = new ThirdOauthUserPO();
        thirdOauthUserPO.setId(null);
        thirdOauthUserPO.setCreateBy(null);
        thirdOauthUserPO.setUpdateBy(null);
        thirdOauthUserPO.setUuid(null);
        thirdOauthUserPO.setUsername(null);
        thirdOauthUserPO.setNickname(null);
        thirdOauthUserPO.setAvatar(null);
        thirdOauthUserPO.setBlog(null);
        thirdOauthUserPO.setCompany(null);
        thirdOauthUserPO.setLocation(null);
        thirdOauthUserPO.setEmail(null);
        thirdOauthUserPO.setGender(null);
        thirdOauthUserPO.setSource(null);
        thirdOauthUserPO.setToken(null);
        thirdOauthUserPO.setRawUserInfo(null);
        thirdOauthUserPO.setUserId(null);
        thirdOauthUserPO.setState(null);
        return thirdOauthUserPO;
    }
    
    private ThirdOauthUserQuery buildThirdOauthUserQuery() {
        ThirdOauthUserQuery thirdOauthUserQuery = new ThirdOauthUserQuery();
        thirdOauthUserQuery.setCreateBy(null);
        thirdOauthUserQuery.setUpdateBy(null);
        thirdOauthUserQuery.setUuid(null);
        thirdOauthUserQuery.setUsername(null);
        thirdOauthUserQuery.setNickname(null);
        thirdOauthUserQuery.setAvatar(null);
        thirdOauthUserQuery.setBlog(null);
        thirdOauthUserQuery.setCompany(null);
        thirdOauthUserQuery.setLocation(null);
        thirdOauthUserQuery.setEmail(null);
        thirdOauthUserQuery.setGender(null);
        thirdOauthUserQuery.setSource(null);
        thirdOauthUserQuery.setToken(null);
        thirdOauthUserQuery.setRawUserInfo(null);
        thirdOauthUserQuery.setUserId(null);
        thirdOauthUserQuery.setState(null);
        return thirdOauthUserQuery;
    }
}