package com.sika.code.test.server.business.user.domain.entity;


import com.sika.code.test.server.business.user.pojo.dto.UserDTO;
import com.sika.code.infrastructure.common.base.pojo.domain.entity.BaseStandardEntity;

/**
 * <p>
 * 用户表领域类
 * </p>
 *
 * @author sikadai
 * @since 2021-10-31 17:45:06
 */
public class UserEntity extends BaseStandardEntity<UserDTO> {

   @Override
   protected UserDTO doExecute() {
     return null;
   }
}