package com.sika.code.test.server.business.auth.domain.aggregate;

import com.sika.code.test.server.business.auth.domain.entity.login.LoginEntity;
import com.sika.code.test.server.business.auth.domain.entity.register.RegisterEntity;
import com.sika.code.test.server.business.auth.domain.factory.AuthFactory;
import com.sika.code.test.server.business.auth.pojo.dto.LoginUserDTO;
import com.sika.code.test.server.business.auth.pojo.request.LoginRequest;
import com.sika.code.test.server.business.auth.pojo.request.RegisterRequest;
import com.sika.code.test.server.business.user.domain.factory.UserFactory;
import com.sika.code.test.server.business.user.pojo.dto.UserDTO;

/**
 * 用户聚合根
 */
public class AuthAggregateRoot {

    /**
     * 用户登录
     */
    public LoginUserDTO login(LoginRequest request) {
        LoginEntity loginEntity = AuthFactory.createLoginEntity(request);
        return loginEntity.execute();
    }

    /**
     * 用户注册
     */
    public UserDTO register(RegisterRequest request) {
        RegisterEntity registerEntity = AuthFactory.createRegisterEntity(request);
        return registerEntity.execute();
    }
}
