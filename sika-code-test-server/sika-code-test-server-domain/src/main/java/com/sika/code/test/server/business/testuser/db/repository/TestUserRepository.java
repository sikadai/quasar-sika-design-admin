package com.sika.code.test.server.business.testuser.db.repository;

import com.sika.code.infrastructure.common.base.repository.BaseRepository;
import com.sika.code.test.server.business.testuser.pojo.po.TestUserPO;

public interface TestUserRepository extends BaseRepository<Long, TestUserPO> {
    int updateBySharingKey(TestUserPO po);
}
