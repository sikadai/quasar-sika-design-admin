package com.sika.code.test.server.business.thirdoauthuser.db.repository.impl;

import com.sika.code.test.server.business.thirdoauthuser.pojo.po.ThirdOauthUserPO;
import com.sika.code.test.server.business.thirdoauthuser.db.mapper.ThirdOauthUserMapper;
import com.sika.code.test.server.business.thirdoauthuser.db.repository.ThirdOauthUserRepository;
import com.sika.code.infractructure.db.repository.impl.BaseRepositoryMyBatisPlus;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 第三方授权用户表 持久化操作实现类
 * </p>
 *
 * @author sikadai
 * @since 2021-11-11 00:22:33
 */
@Repository
public class ThirdOauthUserRepositoryImpl extends BaseRepositoryMyBatisPlus<Long, ThirdOauthUserPO, ThirdOauthUserMapper> implements ThirdOauthUserRepository {

}

