package com.sika.code.test.server.business.testuser.domain.factory;

import com.sika.code.infrastructure.common.base.pojo.domain.factory.BaseFactory;
import com.sika.code.test.server.business.testuser.db.repository.TestUserRepository;
import com.sika.code.test.server.business.testuser.domain.entity.UpdateUserNameEntity;
import com.sika.code.test.server.business.testuser.pojo.valueobject.UpdateUserNameValueObject;

/**
 * @author daiqi
 * @create 2021-10-21 1:07
 */
public interface TestUserFactory {

    static TestUserRepository getTestUserRepository() {
        return BaseFactory.getBeanObj(TestUserRepository.class);
    }

    static UpdateUserNameEntity getUpdateUserNameEntity(UpdateUserNameValueObject valueObject) {
        return UpdateUserNameEntity.builder().updateUserNameValueObject(valueObject).build();
    }
}
