package com.sika.code.test.server.business.testuser.db.mapper;

import com.sika.code.infractructure.db.mapper.BaseMapper;
import com.sika.code.test.server.business.testuser.pojo.po.TestUserPO;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.Collection;

/**
 * <p>
 * 测试用户表 Mapper 接口
 * </p>
 *
 * @author daiqi
 * @since 2021-07-03 15:44:04
 */
@Repository
public interface TestUserMapper extends BaseMapper<Long, TestUserPO> {
    int insertBatchSelective(@Param("list") Collection<TestUserPO> entities);
    int updateBySharingKey(TestUserPO po);
}
