package com.sika.code.test.server.business.auth.domain.entity.login;

import com.sika.code.test.server.business.auth.pojo.dto.LoginUserDTO;

/**
 * 用户登录实体-第三方授权登录
 */
public class LoginThirdPartyAuthorizationEntity extends LoginEntity {
    @Override
    protected void verifyInitBefore() {
    }

    @Override
    protected void verifyLoginCredentials() {

    }

    @Override
    protected LoginUserDTO doExecute() {
        throw new RuntimeException("不支持的登录方式");
    }
}
