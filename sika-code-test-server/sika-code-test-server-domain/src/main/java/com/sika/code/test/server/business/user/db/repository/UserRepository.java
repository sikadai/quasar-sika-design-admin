package com.sika.code.test.server.business.user.db.repository;

import com.sika.code.infrastructure.common.base.repository.BaseRepository;
import com.sika.code.test.server.business.auth.db.repository.AuthRepository;
import com.sika.code.test.server.business.user.pojo.po.UserPO;

/**
 * <p>
 * 用户表 持久化操作类
 * </p>
 *
 * @author sikadai
 * @since 2021-10-31 17:45:05
 */
public interface UserRepository extends AuthRepository, BaseRepository<Long, UserPO> {

}
