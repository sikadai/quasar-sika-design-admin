package com.sika.code.test.server.business.user.domain.factory;


import cn.hutool.core.lang.Assert;
import com.sika.code.infrastructure.common.base.pojo.domain.factory.BaseFactory;
import com.sika.code.test.server.business.auth.constant.LoginTypeEnum;
import com.sika.code.test.server.business.auth.pojo.request.LoginRequest;
import com.sika.code.test.server.business.user.db.repository.UserRepository;
import com.sika.code.test.server.business.user.domain.aggregate.UserAggregateRoot;
import com.sika.code.test.server.business.user.pojo.query.UserQuery;

/**
 * <p>
 * 用户表工厂类
 * </p>
 *
 * @author sikadai
 * @since 2021-10-31 17:45:08
 */
public class UserFactory {
    private static volatile UserRepository userRepository;

    public static UserAggregateRoot createUserAggregateRoot() {
        return BaseFactory.newInstance(UserAggregateRoot.class);
    }

    /**
     * <p>
     * 创建用户登录的查询对象
     * </p>
     *
     * @param request
     * @return com.sika.code.test.server.business.user.pojo.query.UserQuery
     * @author daiqi
     * @date 2021/11/8 23:57
     */
    public static UserQuery createUserLoginQuery(LoginRequest request) {
        Integer loginType = request.getLoginType();
        Assert.notBlank(request.getLoginCredentials(), "请输入登录凭据");
        UserQuery userQuery = new UserQuery();
        if (LoginTypeEnum.isUsernamePwd(loginType)) {
            userQuery.setUsername(request.getLoginCredentials());
        } else if (LoginTypeEnum.isPhoneSmsCode(loginType)) {
            userQuery.setPhone(request.getLoginCredentials());
        } else if (LoginTypeEnum.isMailCode(loginType)) {
            userQuery.setEmail(request.getLoginCredentials());
        } else {
            Assert.isTrue(false, "没有匹配的登录类型【{}】", loginType);
        }
        return userQuery;
    }

}