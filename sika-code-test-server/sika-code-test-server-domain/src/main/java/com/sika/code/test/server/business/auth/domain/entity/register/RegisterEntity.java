package com.sika.code.test.server.business.auth.domain.entity.register;

import com.sika.code.infrastructure.common.base.pojo.domain.entity.BaseStandardEntity;
import com.sika.code.test.server.business.auth.db.repository.AuthRepository;
import com.sika.code.test.server.business.auth.pojo.request.RegisterRequest;
import com.sika.code.test.server.business.user.pojo.dto.UserDTO;
import lombok.Data;
import lombok.experimental.Accessors;

/**
 * <p>
 * 用户登录实体
 * </p>
 *
 * @author daiqi
 * @date 2021/11/8 23:51
 */
@Data
@Accessors(chain = true)
public abstract class RegisterEntity extends BaseStandardEntity<UserDTO> {
    protected RegisterRequest request;
    protected AuthRepository authRepository;

    @Override
    protected void executeBefore() {
        verifyInitBefore();
        init();
        verifyInitAfter();
    }

    protected void init() {
    }

    protected abstract void verifyInitBefore();

    protected void verifyInitAfter() {
        verifyUserPo();
    }

    protected void verifyUserPo() {
        verifyUserExist();
        verifyUserState();
    }

    protected void verifyUserExist() {
    }

    protected void verifyUserState() {
    }

}
