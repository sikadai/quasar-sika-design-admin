package com.sika.code.test.server.business.user.db.mapper;

import com.sika.code.test.server.business.user.pojo.po.UserPO;
import com.sika.code.infractructure.db.mapper.BaseMapper;

/**
 * <p>
 * 用户表 Mapper 接口
 * </p>
 *
 * @author sikadai
 * @since 2021-10-31 17:45:04
 */
public interface UserMapper extends BaseMapper<Long, UserPO> {

}
