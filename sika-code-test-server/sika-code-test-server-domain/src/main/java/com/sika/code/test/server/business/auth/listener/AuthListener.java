package com.sika.code.test.server.business.auth.listener;

import cn.dev33.satoken.listener.SaTokenListener;
import cn.dev33.satoken.stp.SaLoginModel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * 认证监听器
 *
 * @author daiqi
 * @create 2021-11-17 23:26
 */
@Component
@Slf4j
public class AuthListener implements SaTokenListener {

    @Override
    public void doLogin(String loginType, Object loginId, SaLoginModel loginModel) {
        log.info("doLogin-loginType【{}】，loginId【{}】，loginModel【{}】", loginType, loginId, loginModel);
    }

    @Override
    public void doLogout(String loginType, Object loginId, String tokenValue) {
        log.info("doLogout-loginType【{}】，loginId【{}】，tokenValue【{}】", loginType, loginId, tokenValue);
    }

    @Override
    public void doKickout(String loginType, Object loginId, String tokenValue) {
        log.info("doKickout-loginType【{}】，loginId【{}】，tokenValue【{}】", loginType, loginId, tokenValue);

    }

    @Override
    public void doReplaced(String loginType, Object loginId, String tokenValue) {
        log.info("doReplaced-loginType【{}】，loginId【{}】，tokenValue【{}】", loginType, loginId, tokenValue);

    }

    @Override
    public void doDisable(String loginType, Object loginId, long disableTime) {
        log.info("doDisable-loginType【{}】，loginId【{}】，disableTime【{}】", loginType, loginId, disableTime);

    }

    @Override
    public void doUntieDisable(String loginType, Object loginId) {

        log.info("doUntieDisable-loginType【{}】，loginId【{}】", loginType, loginId);
    }

    @Override
    public void doCreateSession(String id) {
        log.info("doCreateSession-id【{}】", id);

    }

    @Override
    public void doLogoutSession(String id) {
        log.info("doLogoutSession-id【{}】", id);

    }
}
