package com.sika.code.test.server.business.thirdoauthuser.domain.entity;


import com.sika.code.test.server.business.thirdoauthuser.pojo.dto.ThirdOauthUserDTO;
import com.sika.code.infrastructure.common.base.pojo.domain.entity.BaseStandardEntity;

/**
 * <p>
 * 第三方授权用户表领域类
 * </p>
 *
 * @author sikadai
 * @since 2021-11-11 00:22:34
 */
public class ThirdOauthUserEntity extends BaseStandardEntity<ThirdOauthUserDTO> {

   @Override
   protected ThirdOauthUserDTO doExecute() {
     return null;
   }
}