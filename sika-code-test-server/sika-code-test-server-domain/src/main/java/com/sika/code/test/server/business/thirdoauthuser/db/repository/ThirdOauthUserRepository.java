package com.sika.code.test.server.business.thirdoauthuser.db.repository;

import com.sika.code.test.server.business.thirdoauthuser.pojo.po.ThirdOauthUserPO;
import com.sika.code.infrastructure.common.base.repository.BaseRepository;

/**
 * <p>
 * 第三方授权用户表 持久化操作类
 * </p>
 *
 * @author sikadai
 * @since 2021-11-11 00:22:33
 */
public interface ThirdOauthUserRepository extends BaseRepository<Long, ThirdOauthUserPO> {

}
