package com.sika.code.test.server.business.auth.domain.entity.login;

import cn.hutool.core.lang.Assert;
import com.sika.code.test.server.business.auth.constant.LoginCodeEnum;

public class LoginMailCodeEntity extends LoginEntity {
    @Override
    protected void verifyInitBefore() {
        Assert.notBlank(userLoginRequest.getLoginCredentials(), "请输入邮箱");
        Assert.notBlank(userLoginRequest.getLoginKey(), "请输入验证码");
    }

    @Override
    protected void verifyLoginCredentials() {
        // 根据验证码校验登录信息
        verifyLoginByCode(userLoginRequest.getLoginCredentials(), LoginCodeEnum.MAIL_CODE);
    }

}
