package com.sika.code.test.server.business.user.db.repository.impl;

import com.google.common.collect.Maps;
import com.sika.code.infractructure.db.repository.impl.BaseRepositoryMyBatisPlus;
import com.sika.code.test.server.business.auth.constant.LoginCodeEnum;
import com.sika.code.test.server.business.auth.pojo.dto.LoginUserDTO;
import com.sika.code.test.server.business.auth.pojo.request.LoginRequest;
import com.sika.code.test.server.business.user.converter.UserConverter;
import com.sika.code.test.server.business.user.db.mapper.UserMapper;
import com.sika.code.test.server.business.user.db.repository.UserRepository;
import com.sika.code.test.server.business.user.domain.factory.UserFactory;
import com.sika.code.test.server.business.user.pojo.po.UserPO;
import com.sika.code.test.server.business.user.pojo.query.UserQuery;
import org.springframework.stereotype.Repository;

import java.util.Map;

/**
 * <p>
 * 用户表 持久化操作实现类
 * </p>
 *
 * @author sikadai
 * @since 2021-10-31 17:45:05
 */
@Repository
public class UserRepositoryImpl extends BaseRepositoryMyBatisPlus<Long, UserPO, UserMapper> implements UserRepository {
    private static final Map<String, String> LOGIN_CODE_MAP = Maps.newConcurrentMap();

    static {
        LOGIN_CODE_MAP.put(LoginCodeEnum.PHONE_SMS_CODE.buildFullKey("15312083732"), "123456");
        LOGIN_CODE_MAP.put(LoginCodeEnum.MAIL_CODE.buildFullKey("meetrice@qq.com"), "123456");
    }


    @Override
    public String getLoginCodeByKey(String key, LoginCodeEnum loginCodeEnum) {
        String fullKey = loginCodeEnum.buildFullKey(key);
        return LOGIN_CODE_MAP.get(fullKey);
    }

    @Override
    public String saveLoginCode(String key, String code, LoginCodeEnum loginCodeEnum) {
        String fullKey = loginCodeEnum.buildFullKey(key);
        return LOGIN_CODE_MAP.put(fullKey, code);
    }

    @Override
    public LoginUserDTO findLoginUser(LoginRequest userLoginRequest) {
        UserQuery userQuery = UserFactory.createUserLoginQuery(userLoginRequest);
        UserPO userPo = find(userQuery);
        return UserConverter.INSTANCE.convertToLoginUserDto(userPo);
    }
}

