package com.sika.code.test.server.business.auth.domain.entity.login;

import cn.hutool.core.lang.Assert;
import cn.hutool.core.util.StrUtil;
import com.sika.code.test.server.business.user.util.UserUtil;

/**
 * 用户登录实体-用户名密码登录
 */
public class LoginUsernamePwdEntity extends LoginEntity {
    @Override
    protected void verifyInitBefore() {
        Assert.notBlank(userLoginRequest.getLoginCredentials(), "请输入用户名");
        Assert.notBlank(userLoginRequest.getLoginKey(), "请输入密码");
    }

    @Override
    protected void verifyLoginCredentials() {
        // 1. 对客户端的密码进行加密
        String pwdFromClient = userLoginRequest.getLoginKey();
        String pwdEncrypt = UserUtil.encryptPassword(pwdFromClient);
        // 2. 比较数据库中的密码与加密后的密码
        boolean equalPwd = StrUtil.equals(loginUserDTO.getPassword(), pwdEncrypt);
        Assert.isTrue(equalPwd, "用户名或密码不正确");
    }

}
