package com.sika.code.test.server.business.thirdoauthuser.domain.factory;


import com.sika.code.test.server.business.thirdoauthuser.db.repository.ThirdOauthUserRepository;
import com.sika.code.infrastructure.common.base.pojo.domain.factory.BaseFactory;

/**
 * <p>
 * 第三方授权用户表工厂类
 * </p>
 *
 * @author sikadai
 * @since 2021-11-11 00:22:34
 */
public interface ThirdOauthUserFactory {

    static ThirdOauthUserRepository getThirdOauthUserRepository() {
        return BaseFactory.getBeanObj(ThirdOauthUserRepository.class);
    }
}