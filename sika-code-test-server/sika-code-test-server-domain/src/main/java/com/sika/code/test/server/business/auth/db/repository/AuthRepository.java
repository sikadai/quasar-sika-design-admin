package com.sika.code.test.server.business.auth.db.repository;

import com.sika.code.test.server.business.auth.constant.LoginCodeEnum;
import com.sika.code.test.server.business.auth.pojo.dto.LoginUserDTO;
import com.sika.code.test.server.business.auth.pojo.request.LoginRequest;

/**
 * @author daiqi
 * @create 2021-11-16 0:56
 */
public interface AuthRepository {
    /**
     * 获取登录验证码 - key
     */
    String getLoginCodeByKey(String key, LoginCodeEnum loginCodeEnum);

    /**
     * 保存登录验证码
     */
    String saveLoginCode(String key, String code, LoginCodeEnum loginCodeEnum);

    /**
     * <p>
     * 获取登录用户信息
     * </p>
     *
     * @param userLoginRequest
     * @return com.sika.code.test.server.business.user.pojo.po.UserPO
     * @author daiqi
     * @date 2021/11/8 23:56
     */
    LoginUserDTO findLoginUser(LoginRequest userLoginRequest);
}
