package com.sika.code.test.server.business.auth.domain.permission;

import cn.dev33.satoken.stp.StpInterface;
import org.assertj.core.util.Lists;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * 权限服务实现类
 *
 * @author daiqi
 * @create 2021-11-17 0:06
 */
@Component
public class PermissionServiceImpl implements StpInterface {
    @Override
    public List<String> getPermissionList(Object loginId, String loginType) {
        List<String> list = Lists.newArrayList();
        list.add("101");
        list.add("user-add");
        list.add("user-delete");
        list.add("user-update");
        list.add("user-get");
        list.add("article-get");
        return list;
    }

    @Override
    public List<String> getRoleList(Object loginId, String loginType) {
        List<String> list = Lists.newArrayList();
        list.add("admin");
        list.add("super-admin");
        return list;
    }
}
