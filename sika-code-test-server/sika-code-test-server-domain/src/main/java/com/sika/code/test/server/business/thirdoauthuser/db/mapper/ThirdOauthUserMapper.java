package com.sika.code.test.server.business.thirdoauthuser.db.mapper;

import com.sika.code.test.server.business.thirdoauthuser.pojo.po.ThirdOauthUserPO;
import com.sika.code.infractructure.db.mapper.BaseMapper;

/**
 * <p>
 * 第三方授权用户表 Mapper 接口
 * </p>
 *
 * @author sikadai
 * @since 2021-11-11 00:22:32
 */
public interface ThirdOauthUserMapper extends BaseMapper<Long, ThirdOauthUserPO> {

}
