package com.sika.code.test.server.business.auth.constant;

import com.sika.code.infrastructure.common.base.constant.BaseTypeEnum;
import com.sika.code.test.server.business.auth.db.repository.AuthRepository;
import com.sika.code.test.server.business.user.db.repository.UserRepository;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 认证的用户类型枚举
 *
 * @author daiqi
 * @create 2021-11-19 0:41
 */
@Getter
@AllArgsConstructor
public enum AuthUserTypeEnum implements BaseTypeEnum<Integer> {
    USER(10, UserRepository.class, "普通用户登录类型"),
    ADMIN(20, UserRepository.class, "管理员登录类型"),
    ;
    private Integer type;
    private Class<? extends AuthRepository> authRepository;
    private String desc;
}
