package com.sika.code.test.server.business.testuser.db.repository.impl;

import com.sika.code.infractructure.db.repository.impl.BaseRepositoryMyBatisPlus;
import com.sika.code.test.server.business.testuser.db.mapper.TestUserMapper;
import com.sika.code.test.server.business.testuser.db.repository.TestUserRepository;
import com.sika.code.test.server.business.testuser.pojo.po.TestUserPO;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class TestUserRepositoryMyBatisPlus extends BaseRepositoryMyBatisPlus<Long, TestUserPO, TestUserMapper> implements TestUserRepository {

    @Override
    public int insertBatch(List<TestUserPO> testUserPOS) {
        return getMapper().insertBatchSelective(testUserPOS);
    }

    @Override
    public int updateBySharingKey(TestUserPO po) {
        return getMapper().updateBySharingKey(po);
    }
}
