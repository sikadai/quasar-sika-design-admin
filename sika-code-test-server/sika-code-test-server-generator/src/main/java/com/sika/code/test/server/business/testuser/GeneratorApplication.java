package com.sika.code.test.server.business.testuser;

import com.sika.code.infractructure.generator.GeneratorDTO;
import com.sika.code.infractructure.generator.GeneratorExecutor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GeneratorApplication {
    public static void main(String[] args) {
        GeneratorDTO generatorDTO = new GeneratorDTO();
        generatorDTO.setInfrastructureOutDir(buildFullPathForJava("infrastructure"));
        generatorDTO.setMapperXmlOutDir(buildFullPathForXml("domain"));
        generatorDTO.setDomainOutDir(buildFullPathForJava("domain"));
        generatorDTO.setApplicationOutDir(buildFullPathForJava("application"));
        generatorDTO.setInterfacesOutDir(buildFullPathForJava("interfaces"));
        generatorDTO.setTestOutDir(buildFullPathForJavaTest("interfaces"));

        generatorDTO.setInfrastructureParent(modulePackage);
        generatorDTO.setDomainParent(modulePackage);
        generatorDTO.setApplicationParent(modulePackage);
        generatorDTO.setInterfacesParent(modulePackage);
        generatorDTO.setTestParent(modulePackage);

        generatorDTO.setAuthor("sikadai");

        generatorDTO.setTableName("sika_third_oauth_user");
        generatorDTO.setDbPassword("SikaDesignAdmin20201225");
        generatorDTO.setDbUsername("root");
        generatorDTO.setDbUrl("jdbc:mysql://121.89.202.68:3306/sika-design-admin?useUnicode=true&characterEncoding=UTF-8&serverTimezone=UTC");
        generatorDTO.setTablePrefix("sika_");
        new GeneratorExecutor(generatorDTO).execute();
    }

    private static String projectPrefix = "sika-code-test-server";
    private static String basePackagePathPrefixForJava = "src/main/java";
    private static String basePackagePathPrefixForJavaTest = "src/test/java";
    private static String basePackagePathPrefixForXml = "src/main/resources";
    private static String modulePackage = "com.sika.code.test.server.business";

    private static String buildFullPathForJava(String projectSuffix) {
        StringBuilder stringBuilder = buildFullPathCore(projectSuffix, basePackagePathPrefixForJava);
        log.info(projectSuffix + "的全路径为：" + stringBuilder);
        return stringBuilder.toString();
    }

    private static String buildFullPathForJavaTest(String projectSuffix) {
        StringBuilder stringBuilder = buildFullPathCore(projectSuffix, basePackagePathPrefixForJavaTest);
        log.info(projectSuffix + "的全路径为：" + stringBuilder);
        return stringBuilder.toString();
    }

    private static String buildFullPathForXml(String projectSuffix) {
        StringBuilder stringBuilder = buildFullPathCore(projectSuffix, basePackagePathPrefixForXml);
        log.info(projectSuffix + "的全路径为：" + stringBuilder);
        return stringBuilder.toString();
    }

    protected static StringBuilder buildFullPathCore(String projectSuffix, String basePackagePathPrefix) {
        StringBuilder stringBuilder = new StringBuilder(System.getProperty("user.dir"));
        stringBuilder
                .append("/sika-code-test-server")
                .append("/")
                .append(projectPrefix)
                .append("-")
                .append(projectSuffix)
                .append("/")
                .append(basePackagePathPrefix);
        return stringBuilder;
    }
}
