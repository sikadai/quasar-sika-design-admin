package com.sika.code.test.server.business.auth.service;

import com.sika.code.test.server.business.auth.pojo.dto.LoginUserDTO;
import com.sika.code.test.server.business.auth.pojo.request.LoginRequest;
import com.sika.code.test.server.business.auth.pojo.request.RegisterRequest;
import com.sika.code.test.server.business.user.pojo.dto.UserDTO;

/**
 * 鉴权服务
 *
 * @author daiqi
 * @create 2021-11-19 0:59
 */
public interface AuthService {
    /**
     * 登录
     */
    LoginUserDTO login(LoginRequest request);

    /**
     * 注册
     */
    UserDTO register(RegisterRequest request);
}
