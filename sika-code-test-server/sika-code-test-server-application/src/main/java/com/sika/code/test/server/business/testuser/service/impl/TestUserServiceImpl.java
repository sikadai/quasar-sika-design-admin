package com.sika.code.test.server.business.testuser.service.impl;

import com.sika.code.infractructure.db.util.HintManagerHandler;
import com.sika.code.infrastructure.common.base.pojo.domain.entity.BaseEntity;
import com.sika.code.infrastructure.common.base.pojo.query.BaseQuery;
import com.sika.code.infrastructure.common.base.service.BaseServiceImpl;
import com.sika.code.test.server.business.testuser.db.repository.TestUserRepository;
import com.sika.code.test.server.business.testuser.domain.factory.TestUserFactory;
import com.sika.code.test.server.business.testuser.pojo.dto.TestUserDTO;
import com.sika.code.test.server.business.testuser.pojo.po.TestUserPO;
import com.sika.code.test.server.business.testuser.pojo.valueobject.UpdateUserNameValueObject;
import com.sika.code.test.server.business.testuser.service.TestUserService;
import org.apache.shardingsphere.api.hint.HintManager;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 测试用户表 服务实现类
 * </p>
 *
 * @author daiqi
 * @since 2021-07-03 15:44:05
 */
@Service(value = "testUserService")
public class TestUserServiceImpl extends BaseServiceImpl<Long, TestUserPO, TestUserDTO, TestUserRepository> implements TestUserService {

    @Override
    public TestUserDTO updateUserName(UpdateUserNameValueObject valueObject) {
        return TestUserFactory.getUpdateUserNameEntity(valueObject).execute();
    }

    @Override
    public <QUERY extends BaseQuery<Long>> TestUserDTO find(QUERY query) {
        try (HintManager hintManager = HintManagerHandler.getInstanceBeforeClear()) {
            HintManagerHandler.addDatabaseShardingValue(hintManager, "01");
            HintManagerHandler.addTableShardingValue(hintManager, "2");
            return super.find(query);
        }
    }
}

