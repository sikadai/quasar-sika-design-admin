package com.sika.code.test.server.business.user.service;

import com.sika.code.infrastructure.common.base.service.BaseService;
import com.sika.code.test.server.business.user.pojo.dto.UserDTO;

/**
 * <p>
 * 用户表 服务类
 * </p>
 *
 * @author sikadai
 * @since 2021-10-31 17:45:09
 */
public interface UserService extends BaseService<Long, UserDTO> {
}
