package com.sika.code.test.server.business.thirdoauthuser.service;

import com.sika.code.test.server.business.thirdoauthuser.pojo.dto.ThirdOauthUserDTO;
import com.sika.code.infrastructure.common.base.service.BaseService;

/**
 * <p>
 * 第三方授权用户表 服务类
 * </p>
 *
 * @author sikadai
 * @since 2021-11-11 00:22:36
 */
public interface ThirdOauthUserService extends BaseService<Long, ThirdOauthUserDTO> {

}
