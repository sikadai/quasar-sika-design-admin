package com.sika.code.test.server.business.testuser.service;


import com.sika.code.infrastructure.common.base.service.BaseService;
import com.sika.code.test.server.business.testuser.pojo.dto.TestUserDTO;
import com.sika.code.test.server.business.testuser.pojo.valueobject.UpdateUserNameValueObject;

/**
 * <p>
 * 测试用户表 服务类
 * </p>
 *
 * @author daiqi
 * @since 2021-07-03 15:44:05
 */
public interface TestUserService extends BaseService<Long, TestUserDTO> {

    TestUserDTO updateUserName(UpdateUserNameValueObject valueObject);
}
