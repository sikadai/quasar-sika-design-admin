package com.sika.code.test.server.business.user.service.impl;

import com.sika.code.infrastructure.common.base.service.BaseServiceImpl;
import com.sika.code.test.server.business.user.db.repository.UserRepository;
import com.sika.code.test.server.business.user.pojo.dto.UserDTO;
import com.sika.code.test.server.business.user.pojo.po.UserPO;
import com.sika.code.test.server.business.user.service.UserService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户表 服务实现类
 * </p>
 *
 * @author sikadai
 * @since 2021-10-31 17:45:09
 */
@Service
public class UserServiceImpl extends BaseServiceImpl<Long, UserPO, UserDTO, UserRepository> implements UserService {

}
