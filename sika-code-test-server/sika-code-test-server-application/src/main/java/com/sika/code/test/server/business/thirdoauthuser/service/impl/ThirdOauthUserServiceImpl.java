package com.sika.code.test.server.business.thirdoauthuser.service.impl;

import com.sika.code.test.server.business.thirdoauthuser.pojo.dto.ThirdOauthUserDTO;
import com.sika.code.test.server.business.thirdoauthuser.pojo.po.ThirdOauthUserPO;
import com.sika.code.test.server.business.thirdoauthuser.db.repository.ThirdOauthUserRepository;
import com.sika.code.test.server.business.thirdoauthuser.service.ThirdOauthUserService;
import com.sika.code.infrastructure.common.base.service.BaseServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 第三方授权用户表 服务实现类
 * </p>
 *
 * @author sikadai
 * @since 2021-11-11 00:22:36
 */
@Service
public class ThirdOauthUserServiceImpl extends BaseServiceImpl<Long, ThirdOauthUserPO, ThirdOauthUserDTO, ThirdOauthUserRepository> implements ThirdOauthUserService {

}
