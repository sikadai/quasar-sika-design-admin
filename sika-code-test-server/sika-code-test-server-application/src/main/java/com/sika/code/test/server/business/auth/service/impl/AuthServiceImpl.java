package com.sika.code.test.server.business.auth.service.impl;

import com.sika.code.test.server.business.auth.domain.factory.AuthFactory;
import com.sika.code.test.server.business.auth.pojo.dto.LoginUserDTO;
import com.sika.code.test.server.business.auth.pojo.request.LoginRequest;
import com.sika.code.test.server.business.auth.pojo.request.RegisterRequest;
import com.sika.code.test.server.business.auth.service.AuthService;
import com.sika.code.test.server.business.user.pojo.dto.UserDTO;
import org.springframework.stereotype.Service;

/**
 * @author daiqi
 * @create 2021-11-19 0:59
 */
@Service
public class AuthServiceImpl implements AuthService {
    @Override
    public LoginUserDTO login(LoginRequest request) {
        return AuthFactory.createAuthAggregateRoot().login(request);
    }

    @Override
    public UserDTO register(RegisterRequest request) {
        return AuthFactory.createAuthAggregateRoot().register(request);
    }
}
